<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>CIU Student Panel</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>CIU Student Panel</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">     

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php
                        if((array_key_exists('image',$_SESSION)&& (!empty($_SESSION['image'])))) {?>
                        <!--<img src="../../../ems-admin/images/profile-image/<?php echo $_SESSION['image'];?>" class="user-image" alt="User Image">-->
                        <?php
                        }
                        else{?>
                            <!--<img src="../../../ems-admin/images/profile-image/1.gif" class="user-image" alt="User Image">-->
                        <?php
                        }
                        ?>
                        <span class="hidden-xs">
                            <?php
                            if((array_key_exists('studentName',$_SESSION)&& (!empty($_SESSION['studentName'])))){
                                echo '<span class="glyphicon glyphicon-user"></span> &nbsp;'.$_SESSION['studentName'];
                            }
                            ?>
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php
                            if((array_key_exists('image',$_SESSION) && (!empty($_SESSION['image'])))) {?>                           
                            <img src="../../../ems-admin/images/profile-image/<?php echo $_SESSION['image'];?>" class="img-circle" alt="User Image">
                            <?php }
                            else{?>
                                <img src="../../resource/img/user_login.png" class="img-circle" alt="User Image"/>
                            <?php
                            }
                            ?>
                            <p>
                                <?php
                                if((array_key_exists('studentName',$_SESSION)&& (!empty($_SESSION['studentName'])))){
                                    echo $_SESSION['studentName']."-". $_SESSION['studentID'];
                                }
                                ?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="../../views/authetication/logout.php" class="btn btn-default btn-flat"><span class="glyphicon glyphicon-log-out"></span> Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>