<?php
include_once"db_config.php";
class General {

    //Database connection process
    public function __construct() {
        $database = new Database();
        $db = $database->dbConnection();
        $this->conn = $db;
    }

    //Run SQL Query
    public function runQuery($sql) {
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    //Page Redirect
    public function redirect($url) {
        header("Location: $url");
    }

    //Start Delete Process
    public function delete($tableName, $fieldName, $condition) {
        try {
            $stmt = $this->conn->prepare("UPDATE $tableName SET $fieldName='Yes' WHERE $condition");
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    public function deleteFromDB($tableName, $condition) {
        try {
            $stmt = $this->conn->prepare("DELETE FROM $tableName WHERE $condition");
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    public function fileID() {
        $id = "";
        date_default_timezone_set("Asia/Dhaka");
        $date = date("Ymd");
        $time = date("His", time());
        $id = $date . '-' . $time;
        return $id;
    }

    //Image Upload
    public function uploadImage($uploadImage, $fileID, $fileLocation) {
        try {
            if (isset($_FILES['uploadImage'])) {
                $errors = 0;
                $file_name = $_FILES['uploadImage']['name'];
                $file_size = $_FILES['uploadImage']['size'];
                $file_tmp = $_FILES['uploadImage']['tmp_name'];
                $file_type = $_FILES['uploadImage']['type'];
                $file_ext = strtolower(end(explode('.', $_FILES['uploadImage']['name'])));

                $expensions = array("jpeg", "jpg", "png");

                if (in_array($file_ext, $expensions) === false) {
                    throw new Exception("Extension not allowed, please choose a JPEG or PNG file.");
                    $errors = 1;
                }
                if ($file_size > 2097152) {
                    throw new Exception("File size must be exactly 2 MB");
                    $errors = 1;
                }
                if ($errors == 0) {
                    move_uploaded_file($file_tmp, "images/" . $fileLocation . "/" . $fileID . "." . $file_ext);
                    return $fileID . "." . $file_ext;
                }
            }
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    //Start Room Setup
    public function roomSetup($roomNo, $roomName, $floorNo, $buildingNo, $capacity, $isActive) {
        try {
            $general = new General();

            $stmt = $general->runQuery("INSERT INTO ems_room_setup(id,room_no,room_name,capacity,floor_no,building_no,is_active)
										VALUES(:id,:roomNo,:roomName, :floorNo, :buildingNo,:capacity, :isActive)");
            $stmt->bindparam(":id", $id);
            $stmt->bindparam(":roomNo", $roomNo);
            $stmt->bindparam(":roomName", $roomName);
            $stmt->bindparam(":capacity", $capacity);
            $stmt->bindparam(":floorNo", $floorNo);
            $stmt->bindparam(":buildingNo", $buildingNo);
            $stmt->bindparam(":isActive", $isActive);
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    //View All School
    public function viewRoom() {
        try {
            $general = new General();
            $stmt = $general->conn->prepare("SELECT * FROM ems_room_setup");
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    public function masterSetting() {
        $site_title = $site_url = $admin_email = $previous_semester = $current_semester = $next_semester = $current_year = $target_form_sold_undergraduate = $target_form_sold_graduate = NULL;
        $_allData = array();
        $query = "SELECT * FROM `ems_master_settings`";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            switch ($row['master_name']) {
                case 'site_title': {
                        $site_title = $row['master_value'];
                        break;
                    }
                case 'site_url': {
                        $site_url = $row['master_value'];
                        break;
                    }
                case 'admin_email': {
                        $admin_email = $row['master_value'];
                        break;
                    }
                case 'previous_semester': {
                        $previous_semester = $row['master_value'];
                        break;
                    }
                case 'current_semester': {
                        $current_semester = $row['master_value'];
                        break;
                    }
                case 'next_semester': {
                        $next_semester = $row['master_value'];
                        break;
                    }
                case 'current_year': {
                        $current_year = $row['master_value'];
                        break;
                    }
                case 'target_form_sold_undergraduate': {
                        $target_form_sold_undergraduate = $row['master_value'];
                        break;
                    }
                case 'target_form_sold_graduate': {
                        $target_form_sold_graduate = $row['master_value'];
                        break;
                    }
            }
        }
        $_allData = array(
            "site_title" => $site_title,
            "site_url" => $site_url,
            "admin_email" => $admin_email,
            "previous_semester" => $previous_semester,
            "current_semester" => $current_semester,
            "next_semester" => $next_semester,
            "previous_year" => $current_year - 1,
            "current_year" => $current_year,
            "next_year" => $current_year + 1,
            "target_form_sold_undergraduate" => $target_form_sold_undergraduate,
            "target_form_sold_graduate" => $target_form_sold_graduate
        );
        return $_allData;
    }

    public function totalStudent() {
        $_totalStudent = array();
        $query_ug = "SELECT COUNT(*) AS total_student_ug FROM `ems_student_info` WHERE `student_status`=1 AND `program_type`=1 AND `student_name`<>'False'";
        $result_ug = mysql_query($query_ug);
        $row_ug = mysql_fetch_assoc($result_ug);
        $totalStudent_ug = $row_ug['total_student_ug'];

        $query_g = "SELECT COUNT(*) AS total_student_g FROM `ems_student_info` WHERE `student_status`=1 AND `program_type`=2 AND `student_name`<>'False'";
        $result_g = mysql_query($query_g);
        $row_g = mysql_fetch_assoc($result_g);
        $totalStudent_g = $row_g['total_student_g'];

        $_totalStudent = array(
            "total_student_ug" => $totalStudent_ug,
            "total_student_g" => $totalStudent_g
        );
        return $_totalStudent;
    }

    public function advisedStudents($programType, $semester, $year) {
        $_advisedStudent = array();
        $query = "SELECT
                    COUNT(`adv_advising_bill`.`student_id`) AS advised_student
                    FROM
                    `adv_advising_bill`
                    INNER JOIN `ems_student_info` ON `adv_advising_bill`.`student_id` =	`ems_student_info`.`student_id`
                    WHERE `adv_advising_bill`.`program`='$programType'
                    AND `ems_student_info`.`student_status` = 1 AND `adv_advising_bill`.`semester`='$semester'
                    AND `adv_advising_bill`.`year`='$year'";
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        $advisedStudent = $row['advised_student'];

        return $advisedStudent;
    }

    public function nAdvisedStudents($programType, $semester, $year) {
        $_advisedStudent = array();
        $query = "SELECT
                    COUNT(`adv_advising_bill`.`student_id`) AS advised_student
                    FROM
                    `adv_advising_bill`
                    INNER JOIN `ems_student_info` ON `adv_advising_bill`.`student_id` =	`ems_student_info`.`student_id`
                    WHERE `adv_advising_bill`.`program`='$programType'
                    AND `adv_advising_bill`.`semester`='$semester'
                    AND `ems_student_info`.`student_status` = 1 AND `adv_advising_bill`.`year`='$year'
                    AND `ems_student_info`.`term_to_begin`='$semester' AND `ems_student_info`.`year`='$year'";

        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        $advisedStudent = $row['advised_student'];

        return $advisedStudent;
    }

    public function schoolWiseAdvisedStudentsNew($school, $programType, $semester, $year) {
        $_advisedStudent = array();
        $query = "SELECT
                    Count(`adv_advising_bill`.`student_id`) AS advised_student
                    FROM
                    `adv_advising_bill`
                    INNER JOIN `ems_student_info` ON `adv_advising_bill`.`student_id` =
                            `ems_student_info`.`student_id`
                    WHERE
                    `adv_advising_bill`.`program` = '$programType' AND
                    `adv_advising_bill`.`semester` = '$semester' AND
                    `adv_advising_bill`.`year` = '$year' AND
                    `ems_student_info`.`term_to_begin` = '$semester' AND
                    `ems_student_info`.`year` = '$year' AND
                    `ems_student_info`.`student_status` = 1 AND
                    `adv_advising_bill`.`school_id`='$school'";

        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        $advisedStudent = $row['advised_student'];

        return $advisedStudent;
    }

    public function schoolWiseAdvisedStudents($school, $programType, $semester, $year) {
        $_advisedStudent = array();
        $query = "SELECT
                    COUNT(`ems_student_info`.`student_id`) AS advised_student,
                    `adv_advising_bill`.`year`,
                    `adv_advising_bill`.`semester`,
                    `adv_advising_bill`.`year`,
                    `adv_advising_bill`.`program`,
                    `adv_advising_bill`.`school_id`,
                    `ems_student_info`.`term_to_begin`
                    FROM
                    `adv_advising_bill`
                    INNER JOIN `ems_student_info` ON `adv_advising_bill`.`student_id` =
                            `ems_student_info`.`student_id`
                    WHERE 
                    `adv_advising_bill`.`year`='$year' AND
                    `adv_advising_bill`.`program`='$programType' AND
                    `adv_advising_bill`.`school_id`='$school' AND 
                    `ems_student_info`.`student_status` = 1 AND
                    `adv_advising_bill`.`semester`='$semester'";
        
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        $advisedStudent = $row['advised_student'];

        return $advisedStudent;
    }

    public function studentsSchoolWise($school, $programType) {

        $sql = "SELECT COUNT(`ems_student_info`.`student_id`) AS studentNumber
					FROM
					`ems_student_info`
					WHERE
					`ems_student_info`.`school_id`='$school' AND
					`ems_student_info`.`student_status` = 1 AND
					`ems_student_info`.`program_type`='$programType'";

        $result = mysql_query($sql);
        $row = mysql_fetch_assoc($result);
        $students = $row['studentNumber'];

        return $students;
    }

    public function studentsSchoolWiseNew($school, $programType, $semester) {

        $sql = "SELECT COUNT(`ems_student_info`.`student_id`) AS studentNumber
					FROM
					`ems_student_info`
					WHERE
					`ems_student_info`.`school_id`='$school' AND
					`ems_student_info`.`student_status` = 1 AND
					`ems_student_info`.`program_type`='$programType' AND
					`ems_student_info`.`term_to_begin`='$semester'";

        $result = mysql_query($sql);
        $row = mysql_fetch_assoc($result);
        $students = $row['studentNumber'];
        return $students;
    }

    //2018.03.06 //getting page title
    public function getPageTitle($file_name) {
        $sql = "SELECT `MenuTitle` FROM `bm_admin_menu` WHERE `MenuLink` = '$file_name'";
        $result = mysql_query($sql);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

}

?>