<?php

require_once('includes/class.general.php');

class Catalogue {

    //Catalogue setup method
    public function catalogueSetup($catalogueName, $schoolID, $majorID, $programID, $effectiveSemester, $effectiveYear, $expireSemester, $expireYear, $rangeEndYear, $rangeBeginYear) {
        try {
            $id = NULL;
            $catalogueID = $this->catalogueID($programID, $rangeBeginYear, $rangeEndYear, $schoolID, $majorID);
            $general = new General();

            $stmt = $general->runQuery("INSERT INTO cat_catalogue_setup(id,catalogue_id,catalogue_name,school_id,major_id,program_id,effective_semester,effective_year,expire_semester,expire_year,range_end_year,range_begin_year)
			VALUES(:id,:catalogueID,:catalogueName,:schoolID, :majorID, :programID, :effectiveSemester, :effectiveYear, :expireSemester, :expireYear, :rangeEndYear, :rangeBeginYear)");
            $stmt->bindparam(":id", $id);
            $stmt->bindparam(":catalogueID", $catalogueID);
            $stmt->bindparam(":catalogueName", $catalogueName);
            $stmt->bindparam(":schoolID", $schoolID);
            $stmt->bindparam(":majorID", $majorID);
            $stmt->bindparam(":programID", $programID);
            $stmt->bindparam(":effectiveSemester", $effectiveSemester);
            $stmt->bindparam(":effectiveYear", $effectiveYear);
            $stmt->bindparam(":expireSemester", $expireSemester);
            $stmt->bindparam(":expireYear", $expireYear);
            $stmt->bindparam(":rangeEndYear", $rangeEndYear);
            $stmt->bindparam(":rangeBeginYear", $rangeBeginYear);
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    //generate CatalogueID
    public function catalogueID($programID, $rangeBeginYear, $rangeEndYear, $schoolID, $majorID) {
        try {
            switch ($programID) {
                case 1: $program = "U";
                    break;
                case 2: $program = "G";
                    break;
                case 3: $program = "C";
                    break;
                case 4: $program = "D";
                    break;
            }
            $rangeBeginYear = substr($rangeBeginYear, 2, 2);
            $rangeEndYear = substr($rangeEndYear, 2, 2);
            $catID = $program . $rangeBeginYear . $rangeEndYear . $schoolID . $majorID;
            return $catID;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    //Start registration process
    public function foundationCourseInput($catalogueID, $programID, $schoolID, $majorID, $groupID, $courseCode, $courseTitle, $creditHours, $level, $courseOrder, $remarks) {
        try {

            $catalogueID = "U131711";
            $courseID = $catalogueID . $courseCode;
            $general = new General();
            $stmt = $general->runQuery("INSERT INTO cat_foundation_courses(id, course_id, catalogue_id, school_id, major_id, group_id, program_id, course_code, course_title, credit_hours, level, course_order, remarks, set_date)
										VALUES(:id,:courseID,:catalogueID,:schoolID,:majorID,:groupID,:programID,:courseCode,:courseTitle,:creditHours,:level,:courseOrder,:remarks,NOW())");
            $stmt->bindparam(":id", $id);
            $stmt->bindparam(":courseID", $courseID);
            $stmt->bindparam(":catalogueID", $catalogueID);
            $stmt->bindparam(":programID", $programID);
            $stmt->bindparam(":schoolID", $schoolID);
            $stmt->bindparam(":majorID", $majorID);
            $stmt->bindparam(":groupID", $groupID);
            $stmt->bindparam(":courseCode", $courseCode);
            $stmt->bindparam(":courseTitle", $courseTitle);
            $stmt->bindparam(":creditHours", $creditHours);
            $stmt->bindparam(":level", $level);
            $stmt->bindparam(":courseOrder", $courseOrder);
            $stmt->bindparam(":remarks", $remarks);
            $stmt->execute();
            return $courseID;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    //Start New Course INSERT process in ems_course_list data table
    public function courseSetup($courseCode, $courseName, $creditHours, $level, $programID) {
        try {
            $id = NULL;
            $general = new General();
            $courseID = $this->courseID($courseCode);
            $stmt = $general->runQuery("INSERT INTO cat_course_list(id,course_id,course_code,course_name,credit_hours,level,program_id)
										VALUES(:id,:courseID,:courseCode,:courseName, :creditHours, :level,:programID)");
            $stmt->bindparam(":id", $id);
            $stmt->bindparam(":courseID", $courseID);
            $stmt->bindparam(":courseCode", $courseCode);
            $stmt->bindparam(":courseName", $courseName);
            $stmt->bindparam(":creditHours", $creditHours);
            $stmt->bindparam(":level", $level);
            $stmt->bindparam(":programID", $programID);
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    public function courseID($courseCode) {
        $general = new General();
        $stmt = $general->runQuery("SELECT COUNT(*) AS id FROM cat_course_list");
        $stmt->execute();
        $number_of_rows = $stmt->fetchColumn();
        $count = $number_of_rows + 1;
        $id = str_pad($count, 4, "0", STR_PAD_LEFT);
        $courseID = $id . $courseCode;
        return $courseID;
    }

    //View All course for inserting subject with catalogueID
    public function viewAllCourse() {
        try {
            $general = new General();
            $stmt = $general->conn->prepare("SELECT * FROM cat_course_list ");
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    public function viewFoundationCourses($catalogueID, $groupID) {
        try {
            $general = new General();
            $stmt = $general->runQuery("SELECT
                                            `cat_course_list`.`course_code`,
                                            `cat_course_list`.`course_name`,
                                            `cat_course_list`.`credit_hours`,
                                            `cat_course_catalogue`.`course_type`,
                                            `cat_course_catalogue`.`catalogue_id`,
                                            `cat_course_catalogue`.`group_id`,
                                            `cat_group_list`.`group_name`
                                          FROM
                                            `cat_course_catalogue`
                                            INNER JOIN `cat_course_list` ON `cat_course_catalogue`.`course_id` =
                                                  `cat_course_list`.`course_id`
                                            INNER JOIN `cat_catalogue_setup` ON `cat_catalogue_setup`.`catalogue_id` =
                                                  `cat_course_catalogue`.`catalogue_id`
                                            INNER JOIN `cat_group_list` ON `cat_course_catalogue`.`group_id` =
                                                  `cat_group_list`.`id`
                                          WHERE `cat_course_catalogue`.`catalogue_id`='$catalogueID' AND `cat_course_catalogue`.`group_id`='$groupID'");
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    //new student Catalogue Setup

    public function isMajorDeclared($studentID) {
        try {
            $general = new General();
            $stmt = $general->runQuery("UPDATE ems_student_info SET catalogue_id=:status WHERE student_id=:studentID");
            $stmt->bindparam(":studentID", $studentID);
            $stmt->bindparam(":status", $status);
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    //Single Course Information
    public function viewCourse($courseID) {
        try {
            $general = new General();
            $stmt = $general->conn->prepare("SELECT
                                                `cat_course_list`.`course_code`,
                                                `cat_course_list`.`course_name`,
                                                `cat_course_list`.`credit_hours`
                                              FROM
                                                `cat_course_list`
                                              WHERE
                                                `cat_course_list`.`course_id` = '$courseID' ");
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    //Only Single Course Code
    public function ViewCourseCode($courseID) {
        try {
            $general = new General();
            $stmt = $general->conn->prepare("SELECT
                                                `cat_course_list`.`course_code`,
                                                `cat_course_list`.`course_name`,
                                                `cat_course_list`.`credit_hours`
                                              FROM
                                                `cat_course_list`
                                              WHERE
                                                `cat_course_list`.`course_id` = '$courseID' ");
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

}

?>