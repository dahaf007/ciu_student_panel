<!--<footer class="main-footer">
    <div class="pull-right hidden-xs">
        Developed by<strong> CIU SOFTWARE TEAM </strong>
    </div>
    <strong>University Management System</strong> reserved.
    
</footer>-->
<footer class="main-footer text-center">
    <small> &copy; 2018-<?php echo date("Y"); ?> <strong>CIU SOFTWARE TEAM</strong> All Rights Reserved </small>
</footer>