<?php

require_once('../includes/class.general.php');

class Employee {

    //Start New Course INSERT process in ems_course_list data table
    public function employeeSetup($empTitle, $empName, $empDepartment, $schoolID, $empDesignation, $empHiredate, $empType, $empStatus, $fatherName, $motherName, $spouseName, $NID, $gender, $birthDate, $maritalStatus, $religion, $presentAdd, $permanentAdd, $mobile, $phone, $email) {
        try {
            $general = new General();
            //For adjunct faculty employee code format is 1001
            if ($empType == 3) {
                $empCode = $this->adjunctFacultyID();
            } else {
                $empCode = $this->employeeID($empHiredate);
            }

            $empID = $this->employeeID($empHiredate);
            //employee personal info insert
            $officialInfo = $this->officialInfo($empCode, $empTitle, $empName, $empDepartment, $schoolID, $empDesignation, $empHiredate, $empType, $empStatus);

            $stmt = $general->runQuery("INSERT INTO hr_employee_personal_info(emp_code,emp_name,father_name,mother_name,spouse_name,NID_pass_no,gender,birth_date,marital_status,religion,present_address,permanent_address,mobile,phone,email,emp_type)
			VALUES(:empCode,:empName,:fatherName,:motherName,:spouseName,:NID,:gender,:birthDate,:maritalStatus,:religion,:presentAdd,:permanentAdd,:mobile,:phone,:email,:empType)");

            $stmt->bindparam(":empCode", $empCode);
            $stmt->bindparam(":empName", $empName);
            $stmt->bindparam(":fatherName", $fatherName);
            $stmt->bindparam(":motherName", $motherName);
            $stmt->bindparam(":spouseName", $spouseName);
            $stmt->bindparam(":NID", $NID);
            $stmt->bindparam(":gender", $gender);
            $stmt->bindparam(":birthDate", $birthDate);
            $stmt->bindparam(":maritalStatus", $maritalStatus);
            $stmt->bindparam(":religion", $religion);
            $stmt->bindparam(":presentAdd", $presentAdd);
            $stmt->bindparam(":permanentAdd", $permanentAdd);
            $stmt->bindparam(":mobile", $mobile);
            $stmt->bindparam(":phone", $phone);
            $stmt->bindparam(":email", $email);
            $stmt->bindparam(":empType", $empType);
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    public function officialInfo($empCode, $empTitle, $empName, $empDepartment, $schoolID, $empDesignation, $empHiredate, $empType, $empStatus) {
        try {
            //employee office info insert									
            $general = new General();
            $stmt = $general->runQuery("INSERT INTO hr_employee_official_info(emp_code,emp_title,emp_name,emp_department,school_id,emp_designation,emp_hire_date,emp_type,emp_status)
			VALUES(:empCode,:empTitle,:empName,:empDepartment,:schoolID,:empDesignation,:empHiredate,:empType,:empStatus)");

            $stmt->bindparam(":empCode", $empCode);
            $stmt->bindparam(":empTitle", $empTitle);
            $stmt->bindparam(":empName", $empName);
            $stmt->bindparam(":empDepartment", $empDepartment);
            $stmt->bindparam(":schoolID", $schoolID);
            $stmt->bindparam(":empDesignation", $empDesignation);
            $stmt->bindparam(":empHiredate", $empHiredate);
            $stmt->bindparam(":empType", $empType);
            $stmt->bindparam(":empStatus", $empStatus);
            $stmt->execute();
            return $stmt;
        } catch (PDOException $e) {
            $errorMessage = $e->getMessage();
            $_SESSION['errorMessage'] = $errorMessage;
        }
    }

    //Customize employee id
    public function employeeID($empHiredate) {
        $general = new General();
        $stmt = $general->runQuery("SELECT emp_code FROM hr_employee_personal_info WHERE emp_type NOT IN(0,3) ORDER BY id DESC LIMIT 1");
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $idSubStr = substr($row['emp_code'], 4) + 1;
        $rowNum = str_pad($idSubStr, 4, "0", STR_PAD_LEFT);
        $dateValue = $empHiredate;
        $time = strtotime($dateValue);
        $month = date("m", $time);
        $year = date("y", $time);
        $regID = $year . $month . $rowNum;
        return $regID;
    }

    //Adjunct Faculty ID
    public function adjunctFacultyID() {
        $general = new General();
        $stmt = $general->runQuery("SELECT emp_code FROM hr_employee_personal_info WHERE emp_type=3 ORDER BY id DESC LIMIT 1");
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $id = $row['emp_code'] + 1;
        //var_dump($id);die();
        return $id;
    }

    //Faculty Profile
    public function employeeProfile($empCode) {
        $general = new General();
        $stmt = $general->runQuery("SELECT
		`hr_employee_personal_info`.`mobile`,
		`ems_school_setup`.`school_name`,
		`hr_employee_personal_info`.`phone`,
		`hr_employee_official_info`.`emp_code`,
		`hr_employee_personal_info`.`email`,
		`hr_employee_official_info`.`emp_title`,
		`hr_employee_official_info`.`emp_name`,
		`hr_employee_official_info`.`emp_designation`,
		`hr_employee_personal_info`.`father_name`,
		`hr_employee_personal_info`.`mother_name`,
		`hr_employee_personal_info`.`spouse_name`,
		`hr_employee_personal_info`.`NID_pass_no`,
		`hr_employee_personal_info`.`gender`,
		`hr_employee_personal_info`.`birth_date`,
		`hr_employee_personal_info`.`marital_status`,
		`hr_employee_personal_info`.`religion`,
		`hr_emp_designation`.`designation_title`,
		`hr_employee_official_info`.`emp_hire_date`,
		`hr_employee_personal_info`.`email`,
		`hr_employee_personal_info`.`phone`,
		`hr_employee_personal_info`.`mobile`,
		`hr_employee_personal_info`.`permanent_address`,
		`hr_employee_personal_info`.`permanent_address`,
		`hr_employee_personal_info`.`present_address`,
		`hr_employee_official_info`.`school_id`
		FROM
		`hr_employee_official_info`
		INNER JOIN `hr_employee_personal_info`
		ON `hr_employee_official_info`.`emp_code` =
		`hr_employee_personal_info`.`emp_code`
		INNER JOIN `ems_school_setup` ON `ems_school_setup`.`school_id` =
		`hr_employee_official_info`.`school_id`
		INNER JOIN `hr_emp_designation` ON `hr_emp_designation`.`designation_id` =
		`hr_employee_official_info`.`emp_designation`
		WHERE `hr_employee_official_info`.`emp_code` = $empCode");
        $stmt->bindparam(":empCode", $empCode);
        $stmt->execute();
        return $stmt;
    }
}

?>