<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="../../resource/img/logo.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>
                    <?php
                    if ((array_key_exists('studentName', $_SESSION) && (!empty($_SESSION['studentName'])))) {
                        echo $_SESSION['studentName'];
                    }
                    ?>
                    <br /></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header text-center">UNIVERSITY MANAGEMENT SYSTEM</li>
            <li class="treeview">
                <a href="../dashboard/">
                    <i class="fa fa-dashboard"></i> <span>My Profile</span>
                </a>
            </li>
            <!--
                        <li class="treeview">
                            <a href="../profile/">
                                <i class="fa fa-user"></i> <span>My Profile</span>
                            </a>
                        </li>-->

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dollar"></i>
                    <span>Accounts History</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="../accounts/transaction-history.php"><i class="fa fa-circle-o"></i> Transaction History</a></li>
                    <li><a href="../accounts/semester-wise-transaction.php"><i class="fa fa-circle-o"></i> Semester Wise Transaction</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-clock-o"></i>
                    <span>Class Routine</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="../routine/"><i class="fa fa-circle-o"></i> Class Routine</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>Result</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="../result/semester-result.php"><i class="fa fa-circle-o"></i> Semester Result</a></li>
                    <li><a href="../result/view-transcript.php"><i class="fa fa-circle-o"></i> Full Transcript</a></li>
                </ul>
            </li>

            <!--<li class="treeview">
                <a href="#">
                    <i class="fa fa-shopping-cart"></i> <span>Course Advising</span>
                </a>
            </li>-->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list-alt"></i>
                    <span>Evaluation</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="../evaluation/index.php"><i class="fa fa-circle-o"></i> Teacher's Evaluation</a></li>
                </ul>
            </li>


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
