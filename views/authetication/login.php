<?php
include_once('../../vendor/autoload.php');
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$status= $auth->prepare($_POST)->is_registered();

//Check if user is registered or not
if($status){
    $isActive= $auth->prepare($_POST)->is_active();//Check user activity status
    if($isActive){
        $allData= new Auth();
        $loggedInInfo = $allData->prepare($_POST)->loggedInDetails();

        $_SESSION['studentID']=$loggedInInfo['student_id'];
        $_SESSION['studentName'] = $loggedInInfo['student_name'];
        $_SESSION['image']=$loggedInInfo['image'];
        $_SESSION['program']=$loggedInInfo['program_type'];
        return Utility::redirect('../../views/dashboard/index.php');
    }
    else{
        Message::message("<div class='alert alert-danger alert-dismissible'>
                                <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                Inactive user. Please contact with system administrator
                            </div>");        
        return Utility::redirect('../../login.php');
    }
}else{
    Message::message("<p class='alert alert-danger alert-dismissible'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            Wrong username or password
                        </p>");
    return Utility::redirect('../../login.php');
}
