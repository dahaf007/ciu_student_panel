<?php
include_once('../../vendor/autoload.php');
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$status= $auth->prepare($_POST)->is_registered();
//Check if user is registered or not
if($status){
    $isActive= $auth->prepare($_POST)->is_active();//Check user activity status
    if($isActive){
        $allData= new Auth();
        $loggedInInfo = $allData->prepare($_POST)->loggedInDetails();
        $_SESSION['email']=$_POST['email'];
        $_SESSION['userName'] = $loggedInInfo['first_name']." ".$loggedInInfo['last_name'];
        $_SESSION['roleID']=$loggedInInfo['role_id'];
        $_SESSION['employeeID']=$loggedInInfo['employee_id'];
        $_SESSION['createdDate']=$loggedInInfo['created_date'];
        $path = $_SESSION['path'];
        return Utility::redirect($path);
    }
    else{
        Message::message("<span style=\"color:red\">Inactive user. Please contact with system admin</span>");
        return Utility::redirect('../../login.php');
    }

}else{
    Message::message("<span style=\"color:red\">Wrong username or password");
    return Utility::redirect('../../login.php');

}
