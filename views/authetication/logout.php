<?php
session_start();
include_once('../../vendor/autoload.php');

use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$auth= new Auth();
$status= $auth->log_out();

if($status){
    Message::message("<div class='alert alert-success alert-dismissible'>
                            <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            You have been successfully logged-out.
                        </div>");
    return Utility::redirect('../../login.php');
}