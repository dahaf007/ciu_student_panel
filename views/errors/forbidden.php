<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Access Forbidden</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--tab icon-->
        <link rel="icon" href="resource/img/logo.png" type="image/gif" sizes="16x16">
        <link href="../../resource/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="resource/admin/plugins/jQuery/jquery-2.2.3.min.js" type="text/javascript"></script>
        <script src="../../resource/admin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </head>
    <body> 
        <div class="container text-center">    
            <div class="row content">    
                <div class="col-md-12 text-center"> 
                    <h1 class="text-danger">Access Forbidden!</h1>
                    <p class="text-warning">You are not allowed to access this file or folder.</p>
                </div>    
            </div>
        </div>
    </body>
</html>
