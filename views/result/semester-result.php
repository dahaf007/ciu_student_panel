<?php
include_once('../../vendor/autoload.php');
use App\Result\Result;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$loggedIn= $auth->prepare($_POST)->logged_in();//Check user activity status
if(!$loggedIn){
    $_SESSION['loggedInMessage']="<span style=\"color:red;\">You have to log in before enter this page</span>";
    return Utility::redirect('../../login.php');
}

if((array_key_exists('studentID',$_SESSION)&& (!empty($_SESSION['studentID'])))){
    $_POST['studentID']=$_SESSION['studentID'];
}
$semesterTitleWiserStudentTranscript = array();
if (($_SERVER["REQUEST_METHOD"] == "POST") AND isset($_POST['search'])){
    if(isset($_POST['semesterTitle'])){
        $semesterTitleWiserStudentTranscript = new Result();
        $semesterTitleWiserStudentTranscript = $semesterTitleWiserStudentTranscript->prepare($_POST)->semesterTitleWiseTranscript();
    }
}
$semesterTitle = new Result();
$semesterTitle = $semesterTitle->prepare($_POST)->semesterTitle();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Result | UMS</title>
    <?php include_once('../../includes/head.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once('../../includes/header.php');?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include_once('../../includes/sidebar.php');?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Result
            </h1>
        </section>

        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-8">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> Semester: <?php if(isset($_POST['semesterTitle'])){echo $_POST['semesterTitle'];};?>
                    </h2>
                </div>
                <!-- /.col -->
                <form action="" method="POST">
                    <div class="col-xs-3">
                        <div class="form-group">
                            <select class="form-control" name="semesterTitle">
                                <option>Search.....</option>
                                <?php
                                foreach($semesterTitle AS $title):
                                    ?>
                                    <option value="<?php echo $title['semester_title']; ?>"><?php echo $title['semester_title'];?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-1" style="margin:0px; padding:0px;">
                        <div class="form-group">
                            <input type="submit" name="search" class="btn btn-primary"/>
                        </div>
                    </div>
                </form>
            </div>

            <!-- Table row -->
            <div class="row">

                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Course Code</th>
                            <th>Course Title</th>
                            <th>Credit Hour</th>
                            <th>Grade</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach($semesterTitleWiserStudentTranscript AS $transcript): ?>
                            <tr>
                                <td><?php echo $transcript['course_code'];?></td>
                                <td><?php echo $transcript['course_name'];?></td>
                                <td><?php echo $transcript['credit_hour'];?></td>
                                <td><?php echo $transcript['grade'];?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('../../includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include_once('../../includes/script.php');?>
</body>
</html>