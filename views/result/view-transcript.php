<?php
include_once('../../vendor/autoload.php');

use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

include_once('../../includes/config.php');
include_once('../../includes/class.general.php');

$auth = new Auth();
$loggedIn = $auth->prepare($_POST)->logged_in(); //Check user activity status
if (!$loggedIn) {
    $_SESSION['loggedInMessage'] = "<span style=\"color:red;\">You have to log in before enter this page</span>";
    return Utility::redirect('../../login.php');
}

if ((array_key_exists('studentID', $_SESSION) && (!empty($_SESSION['studentID'])))) {
    $student_id = $_SESSION['studentID'];
}

$query = "SELECT MIN(`year`) AS minYear, MAX(`year`) AS maxYear FROM grd_transcript WHERE student_id='$student_id' AND status='2'";
$result = mysql_query($query);
$row = mysql_fetch_array($result);
$minYear = $row['minYear'];
$maxYear = $row['maxYear'];

$general = new General();
$queryForMajorAndMinor = "SELECT
                            `ems_major_setup`.`name` AS `major`,
                            `ems_major_setup1`.`name` AS `minor`,
                            `hr_employee_official_info`.`emp_name`,
                            `hr_employee_official_info`.`emp_code`,
                            `ems_student_info`.`is_major_declared`,
                            `ems_student_info`.`program_type`,
                            `ems_student_info`.`school_id`
                        FROM
                            `ems_student_info`
                        LEFT JOIN `reg_major_minor_declared` 
                            ON `ems_student_info`.`student_id` = `reg_major_minor_declared`.`student_id`
                        LEFT JOIN `ems_major_setup` 
                            ON `reg_major_minor_declared`.`major_id` = `ems_major_setup`.`id`
                        LEFT JOIN `ems_major_setup` `ems_major_setup1`
                            ON `reg_major_minor_declared`.`minor_id` = `ems_major_setup1`.`id`
                        LEFT JOIN `reg_advisor_assign` 
                            ON `reg_major_minor_declared`.`student_id` = `reg_advisor_assign`.`student_id`
                        LEFT JOIN `hr_employee_official_info` 
                            ON `reg_advisor_assign`.`emp_code` = `hr_employee_official_info`.`emp_code`	
                        WHERE
                            `ems_student_info`.`student_id` = '$student_id'";
$stmtForMajorAndMinor = $general->conn->prepare($queryForMajorAndMinor);
$stmtForMajorAndMinor->execute();
$rowForMajorAndMinor = $stmtForMajorAndMinor->fetch(PDO::FETCH_ASSOC);
$programType = $rowForMajorAndMinor['program_type']; //31.01.2018
$schoolID = $rowForMajorAndMinor['school_id']; //31.01.2018
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <title>Result | Full Transcript</title>
        <?php include_once('../../includes/head.php'); ?>
        
        <style type="text/css">
            .table td, .table th {
                padding: 0 !important;
                border-top: none !important;
                background: black;
                color: white;
            }
            @media print {
                html {
                    display: none !important;
                }
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini" oncontextmenu="return false">
        <div class="wrapper">
            <?php include_once('../../includes/header.php'); ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include_once('../../includes/sidebar.php'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Full Transcript (Not for official use)
                    </h1>
                </section>

                <!-- Main content -->
                <section class="invoice">
                    <div class="row">            
                        <table class="table table-dark">
                            <thead>
                                <td>
                                    <table width="100%">
                                        <tr>
                                            <td style="width:10%; padding:5px;"><strong>Course</strong></td>
                                            <td style="width:45%; padding:3px;"><strong>Course Title</strong></td>
                                            <td style="width:10%; padding:3px;"><strong>Type</strong></td>
                                            <td style="width:10%; padding:3px;"><strong>Grade</strong></td>
                                            <td style="width:6%; padding:3px;"><strong>Course Credit</strong></td>
                                            <td style="width:6%; padding:3px;"><strong>Credit Earned</strong></td>
                                            <td style="width:8%; padding:3px;"><strong>Credit for GPA</strong></td>
                                            <td style="width:6%; padding:3px;"><strong>Grade Point</strong></td>
                                        </tr>
                                    </table>
                                </td>
                            </thead>

                            <?php
                            $_allData = array();
                            $totalCreditAttempted = 0.00;
                            $totalCreditsEarned = 0.00;
                            $totalGradePoint = 0.00;
                            for ($year = $minYear; $year <= $maxYear; $year++) {
                                $query_data = "SELECT distinct(semester), semester_title FROM grd_transcript WHERE student_id='$student_id' AND year='$year' AND status=2 ORDER BY semester_order ASC,year ASC";
                                $result_data = mysql_query($query_data);
                                while ($row_data = mysql_fetch_assoc($result_data)) {
                                    $semester = $row_data['semester'];
                                    ?>
                                    <!-- semester result loop -->
                                    <tbody class="avoid-pagebreak">
                                        <tr>
                                            <td style="padding:5px;"><strong><?php echo $row_data['semester_title']; ?></strong></td>
                                        </tr>
                                        <?php
                                        $query_sub = "SELECT
                                                        `grd_transcript`.*,
                                                        `cat_course_list`.`course_code`,
                                                        `cat_course_list`.`course_name`
                                                      FROM
                                                        `grd_transcript`
                                                        INNER JOIN `cat_course_list` ON `grd_transcript`.`course_id` =
                                                              `cat_course_list`.`course_id`
                                                      WHERE student_id='$student_id' AND year='$year' AND semester='$semester' AND status='2'";
                                        $result_sub = mysql_query($query_sub);
                                        $creditHour = 0;
                                        $creditEarned = 0;
                                        $creditForGPA = 0;
                                        $gradePoint = 0;
                                        $gpa = 0;
                                        while ($row_sub = mysql_fetch_assoc($result_sub)) {
                                            $creditHour = $creditHour + $row_sub['credit_hour'];
                                            $creditEarned = $creditEarned + $row_sub['credit_earned'];
                                            $creditForGPA = $creditForGPA + $row_sub['credit_for_gpa'];
                                            $gradePoint = $gradePoint + $row_sub['grade_point'];
                                            ?>
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="width:10%; padding:5px;"><?php echo $row_sub['course_code']; ?></td>
                                                            <td style="width:45%; padding:3px;"><?php echo $row_sub['course_name']; ?></td>
                                                            <td style="width:10%; padding:3px;"><?php echo $row_sub['type']; ?></td>
                                                            <td style="width:10%; padding:3px;"><?php echo $row_sub['grade']; ?></td>
                                                            <td style="width:6%; padding:3px;"><?php echo $row_sub['credit_hour']; ?></td>
                                                            <td style="width:6%; padding:3px;"><?php echo $row_sub['credit_earned']; ?></td>
                                                            <td style="width:8%; padding:3px;"><?php echo $row_sub['credit_for_gpa']; ?></td>
                                                            <td style="width:6%; padding:3px;"><?php echo $row_sub['grade_point']; ?></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width:75%; padding:5px;">Semester Total:</td>
                                                        <td style="width:6%; padding:3px;">
                                                            <strong><?php echo number_format($creditHour, "2"); ?></strong></td>
                                                        <td style="width:6%; padding:3px;">
                                                            <strong><?php echo number_format($creditEarned, "2"); ?></strong></td>
                                                        <td style="width:8%; padding:3px;">
                                                            <strong><?php echo number_format($creditForGPA, "2"); ?></strong></td>
                                                        <td style="width:6%; padding:3px;">
                                                            <strong><?php echo number_format($gradePoint, "2"); ?></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                        if ($gradePoint == 0.00 AND $creditForGPA == 0.00) {
                                                            $gpa = 0.00;
                                                        } else {
                                                            $gpa = (float) $gradePoint / $creditForGPA;
                                                        }
                                                        $totalCreditAttempted = $totalCreditAttempted + $creditForGPA;
                                                        $totalCreditsEarned = $totalCreditsEarned + $creditEarned;
                                                        $totalGradePoint = $totalGradePoint + $gradePoint;
                                                        ?>
                                                        <td style="padding-left: 2cm;">GPA:
                                                            <strong><?php echo number_format($gpa, "2") ?></strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                }
                                ?>
                            </tbody>

                            <!---------------------------------------------------->
                            <!-- For Transfer Institution Name -->
                            <?php
                            $queryForInstitution = "SELECT
                                                        `institution_name`
                                                    FROM
                                                        `grd_transfer_waived` WHERE `student_id` = '$student_id' LIMIT 1";

                            $rowInstitution = mysql_query($queryForInstitution);
                            $institutionName = mysql_fetch_assoc($rowInstitution);
                            ?>

                            <!-- Transfer And Waived Course List -->
                            <?php
                            $queryTransferAndWaived = "SELECT
                                                            `cat_course_list`.`course_code`,
                                                            `cat_course_list`.`course_name`,
                                                            `grd_transfer_waived`.`status`,
                                                            `grd_transfer_waived`.`course_credit`
                                                          FROM
                                                            `grd_transfer_waived`
                                                            INNER JOIN `cat_course_list` ON `grd_transfer_waived`.`course_id` =
                                                              `cat_course_list`.`course_id` 
                                                          WHERE student_id='$student_id'";
                            $resultTransferAndWaived = mysql_query($queryTransferAndWaived);
                            $TransferAndWaivedCreditHour = 0;

                            //Student Transfer And Waived Status
                            $queryForStatus = "SELECT
                                                `grd_transfer_waived`.`status`
                                              FROM
                                                `grd_transfer_waived` WHERE `student_id` = '$student_id' LIMIT 1";

                            $Status = mysql_query($queryForStatus);
                            $StatusName = mysql_fetch_assoc($Status);
                            ?>
                        </table>

                        <section style="width: 1000px" <?php
                        if ($StatusName['status'] == "Transfer" || $StatusName['status'] == "Waived") {
                            echo 'class="avoid-pagebreak-transfer-waved top-bottom-border"';
                        }
                        ?> >
                            <tr>
                                <td>
                                    <table width="100%" class="total-table-border top-bottom-border">

                                        <!--transfer and waived. edit for border purpose.-->
                                        <?php if ($StatusName['status'] == "Transfer") { ?>
                                            <tr>
                                                <td colspan="5">
                                                    <p>&nbsp; TRANSFERRED COURSE/S
                                                        - <?php echo $institutionName['institution_name']; ?></p>
                                                </td>
                                                <td style="width:6%; padding:3px;"></td>
                                                <td style="width:8%; padding:3px;"></td>
                                                <td style="width:6%; padding:3px;"></td>
                                            </tr>
                                        <?php } ?>
                                        <?php if ($StatusName['status'] == "Waived") { ?>
                                            <tr>
                                                <td colspan="5">
                                                    <p>&nbsp; WAIVED COURSE/S</p>
                                                </td>
                                                <td style="width:6%; padding:3px;"></td>
                                                <td style="width:8%; padding:3px;"></td>
                                                <td style="width:6%; padding:3px;"></td>
                                            </tr>
                                        <?php } ?>
                                        <!---->
                                        <?php
                                        while ($rowTransferAndWaived = mysql_fetch_assoc($resultTransferAndWaived)) {
                                            $TransferAndWaivedCreditHour = $TransferAndWaivedCreditHour + $rowTransferAndWaived['course_credit'];
                                            ?>
                                            <tr>
                                                <td style="width:10%; padding:5px;"><?php echo $rowTransferAndWaived['course_code']; ?></td>
                                                <td style="width:45%; padding:3px;"><?php echo $rowTransferAndWaived['course_name']; ?></td>
                                                <td style="width:10%; padding:3px;"><?php echo ''; ?></td>
                                                <td style="width:10%; padding:3px;"><?php echo ''; ?></td>
                                                <td style="width:6%; padding:3px;"><?php echo $rowTransferAndWaived['course_credit']; ?></td>
                                                <td style="width:6%; padding:3px;"><?php echo '0.00'; ?></td>
                                                <td style="width:8%; padding:3px;"><?php echo '0.00'; ?></td>
                                                <td style="width:6%; padding:3px;"><?php echo '0.00'; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </td>
                            </tr>
                            <?php if ($StatusName['status'] == "Transfer" OR $StatusName['status'] == "Waived") { ?>
                                <tr>
                                    <td>
                                        <table width="100%" class="total-table-border" style="border-bottom: none; border-top: none; border-collapse: collapse;">
                                            <tr>
                                                <td style="width:75%; padding:5px;"></td>
                                                <td style="width:6%; padding:3px;">
                                                    <strong><?php echo number_format($TransferAndWaivedCreditHour, "2"); ?></strong>
                                                </td>
                                                <td style="width:6%; padding:3px;"><strong>0.00</strong></td>
                                                <td style="width:8%; padding:3px;"><strong>0.00</strong></td>
                                                <td style="width:6%; padding:3px;"><strong>0.00</strong></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            <?php } ?>
                        </section>
                        <div>
                            <table class="table table-dark">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <?php if ($StatusName['status'] == "Transfer") { ?>
                                                <tr>
                                                    <td style="width:70%">Transfered Credits:</td>
                                                    <td style="width:10%">
                                                        <strong><?php echo number_format($TransferAndWaivedCreditHour, "2"); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <?php if ($StatusName['status'] == "Waived") { ?>
                                                <tr>
                                                    <td style="width:70%">Waived Credits:</td>
                                                    <td style="width:10%">
                                                        <strong><?php echo number_format($TransferAndWaivedCreditHour, "2"); ?></strong>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td style="width:70%">Total Credits Attempted:</td>
                                                <td style="width:10%">
                                                    <strong><?php echo number_format($totalCreditAttempted, "2"); ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td style="width:70%">Total Credits Earned:</td>
                                                <td style="width:10%">
                                                    <strong><?php echo number_format($totalCreditsEarned + $TransferAndWaivedCreditHour, "2"); ?></strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:70%">Total Grade Point:</td>
                                                <td style="width:10%"><strong><?php echo number_format($totalGradePoint, "2"); ?></strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:70%">Cumulative GPA:</td>
                                                <td style="width:10%">
                                                    <strong><?php
                                                        $cgpa = number_format($totalGradePoint / $totalCreditAttempted, "2");
                                                        echo $cgpa;
                                                        ?></strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <br/>
                            <!-- check if student is on probation -->
                            <div>
                                <p> 
                                    <b>
                                        <?php
                                        if ($programType == 1 || ($programType == 2 && $schoolID == 4)) {
                                            if ($cgpa < 2) {
                                                echo '<b>' . '*On probation' . '</b>';
                                            } else {
                                                echo '';
                                            }
                                        } else if ($programType == 2) {
                                            if ($cgpa < 2.5) {
                                                echo '<b>' . '*On probation' . '</b>';
                                            } else {
                                                echo '';
                                            }
                                        }
                                        ?>
                                    </b> 
                                </p>
                            </div>         
                            <br/>                
                        </div>
                    </div>                
                </section>                
            </div>
            <?php include_once('../../includes/footer.php');?>
        </div>            
        <?php include_once('../../includes/script.php');?>
        <script type="text/javascript">            
            $(document).ready(function() {
                function mousehandler(e) {
                    var myevent = (isNS) ? e : event;
                    var eventbutton = (isNS) ? myevent.which : myevent.button;
                    if ((eventbutton == 2) || (eventbutton == 3)) return false;
                }
                document.oncontextmenu = mischandler;
                document.onmousedown = mousehandler;
                document.onmouseup = mousehandler;
                function disableCtrlKeyCombination(e) {
                    var forbiddenKeys = new Array("a", "s", "c", "x","u","p");
                    var key;
                    var isCtrl;
                    if (window.event) {
                        key = window.event.keyCode;
                        //IE
                        if (window.event.ctrlKey)
                            isCtrl = true;
                        else
                            isCtrl = false;
                    }
                    else {
                        key = e.which;
                        //firefox
                        if (e.ctrlKey)
                            isCtrl = true;
                        else
                            isCtrl = false;
                    }
                    if (isCtrl) {
                        for (i = 0; i < forbiddenKeys.length; i++) {
                            //case-insensitive comparation
                            if (forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase()) {
                                return false;
                            }
                        }
                    }
                    return true;
                }
            });
        </script>
    </body>
</html>