<?php

include_once('../../vendor/autoload.php');

use App\User\User;
use App\Utility\Utility;
use App\Message\Message;

$error = false; //no error

if ((isset($_POST['userName']) && empty($_POST['userName'])) OR ( isset($_POST['studentID']) && empty($_POST['studentID'])) OR ( isset($_POST['userPass']) && empty($_POST['userPass'])) OR ( isset($_POST['retypePassword']) && empty($_POST['retypePassword']))) {
    $error = TRUE;
}

if ($error == TRUE) {
    Message::message("<div class='alert alert-danger alert-dismissible'>
                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>Error!</strong> Please fill up required field.
                    </div>");
    return Utility::redirect('sign-up.php');
} elseif ($error == FALSE) {
    if ((isset($_POST['userPass']) && !empty($_POST['userPass'])) AND ( (isset($_POST['retypePassword']) && !empty($_POST['retypePassword'])))) {
        if ($_POST['userPass'] <> $_POST['retypePassword']) {
            Message::message("<div class='alert alert-danger alert-dismissible'>
                                <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                <strong>Error!</strong> Password mismatch
                            </div>");
            return Utility::redirect('sign-up.php');
        } elseif ($_POST['userPass'] === $_POST['retypePassword']) {
            $allData = new User();
            $status = $allData->prepare($_POST)->is_exist();
            $validStudentID = $allData->prepare($_POST)->is_valid_student_id();
            if ($validStudentID == FALSE) {
                Message::message("<div class='alert alert-danger alert-dismissible'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                        <strong>Error!</strong> Student ID is not valid.
                                    </div>");
                return Utility::redirect('sign-up.php');
            }
            
            if ($status) {
                Message::message("<div class='alert alert-danger alert-dismissible'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                        <strong>Error!</strong> Student ID already exists.
                                    </div>");
                return Utility::redirect('sign-up.php');
            } else {
                $obj = new User();
                $obj->prepare($_POST)->store();
                Message::message("<div class='alert alert-success alert-dismissible'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                        <strong>Success!</strong> Your Student Panel Account has been created successfully.
                                    </div>");
                return Utility::redirect('../../login.php');
            }
        }
    }
}