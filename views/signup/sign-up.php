<?php
include_once('../../vendor/autoload.php');

use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

if (session_id() == '') {
    session_start();
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="../../resource/admin/bootstrap/css/bootstrap.min.css">
        <!-- css -->
        <link rel="stylesheet" href="../../resource/admin/css/registration.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../resource/admin/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="../../resource/admin/plugins/iCheck/square/blue.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Student Panel Registration</title>
        <link rel="icon" href="../../resource/img/logo.png" type="image/gif" sizes="16x16"> 
        <style>
            .content-wrapper{
                margin: 0 auto;
                background-image: url("../../resource/img/bg-2.jpeg");
                background-size: cover;
                background-repeat: no-repeat;                
            }
            .vcenter {
                margin-top: 10vh;
            }
            .content{
                height: 96vh;
            }
            .transparent-bg{
                background: transparent;
            }
            .text-white{
                color: white;
            }
            footer{
                height: 4vh;
            }
            .box.box-info {
                border-top-color: #00c0ef00;
                /*background-color: transparent;*/
                background-color: rgba(6, 0, 20, 0.3);
            }
            .transperancy{
                opacity: 0.9;
            }
            .control-label{
                color: white; 
            }
        </style>
    </head>
    <body class="hold-transition skin-blue">
        <div class="wrapper">
            <?php // include_once('../../includes/header.php'); ?>
            <!-- Left side column. contains the logo and sidebar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 vcenter">
                            <!--showing messages start-->
                            <?php
                            if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
                                ?>
                                <div id="message">
                                    <?php echo $_SESSION['message'];
                                    $_SESSION['message'] = "";
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                            <!--showing messages end-->

                            <!-- Horizontal Form -->
                            <div class="box box-info">
                                <div class="box-header with-border">
                                    <h2 class="box-title text-white"><b>Create your Student Panel Account</b></h2>
                                    <p class="text-white">It's free and always will be.</p>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form class="form-horizontal" action="store.php" method="post" enctype="multipart/form-data" >
                                    <div class="box-body">
<!--                                        <div class="form-group">
                                            <label for="userName" class="col-sm-3 control-label">User Name</label>

                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="userName" placeholder="User Name" name="userName" required="required">
                                            </div>
                                        </div>-->

                                        <div class="form-group">
                                            <label for="studentID" class="col-sm-3 control-label">Student ID</label>

                                            <div class="col-sm-9">
                                                <input type="number" class="form-control" id="studentID" placeholder="Student ID" name="studentID" required="required">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="userPass" class="col-sm-3 control-label">Password</label>

                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" id="userPass" placeholder="Password" name="userPass" required="required" minlength="6" title="Password should be at least 6 characters long"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="userPass" class="col-sm-3 control-label">Confirm Password</label>

                                            <div class="col-sm-9">
                                                <input type="password" class="form-control" id="userPass" placeholder="Confirm Password" name="retypePassword" required="required" minlength="6" title="Password should be at least 6 characters long"/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer transparent-bg">
                                        <p class ="text-gray" >By clicking Sign Up, you agree to our Terms, Data Policy and Cookie Policy. You may receive notifications from us and can opt out at any time.</p>
                                        <br/>
                                        <div>
                                            <a href="../../login.php" type="button" class="btn btn-primary pull-left transperancy" title="Go Back"><span class="glyphicon glyphicon-arrow-left"></span></a>
                                            <div class="pull-right">
                                                <button type="reset" class="btn btn-danger transperancy" title="Refresh"><span class="glyphicon glyphicon-refresh"></span></button>                                        
                                                <button type="submit" class="btn btn-success transperancy" title="Sign Up"><span class="glyphicon glyphicon-ok"></span></button>
                                            </div>                                            
                                        </div>
                                    </div>
                                    <!-- /.box-footer -->
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
                <footer class="transparent-bg text-white text-center">
                    <small> &copy; 2018-<?php echo date("Y");?> <strong>CIU SOFTWARE TEAM</strong> All Rights Reserved </small>
<!--                    <div class="pull-right hidden-xs">
                        Developed by<strong> CIU SOFTWARE TEAM </strong>
                    </div>
                    <strong>University Management System</strong> reserved.-->
                </footer>
            </div>
            <!-- /.content-wrapper -->
            
        </div>
        <!-- /.login-box -->

        <!-- jQuery 2.2.3 -->
        <script src="../../resource/admin/plugins/jQuery/jquery-2.2.3.min.js" type="text/javascript"></script>        
        <!-- Bootstrap 3.3.6 -->
        <script src="../../resource/admin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="../../resource/admin/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
        <script>
            $('#message').show().delay(3000).fadeOut();
        </script>
    </body>
</html>