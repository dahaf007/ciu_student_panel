<?php
include_once('../../vendor/autoload.php');
use App\Accounts\Accounts;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$loggedIn= $auth->prepare($_POST)->logged_in();//Check user activity status
if(!$loggedIn){
    $_SESSION['loggedInMessage']="<span style=\"color:red;\">You have to log in before enter this page</span>";
    return Utility::redirect('../../login.php');
}

if((array_key_exists('studentID',$_SESSION)&& (!empty($_SESSION['studentID'])))){
    $_POST['studentID']=$_SESSION['studentID'];
}
$allData = new Accounts();
$allData = $allData->prepare($_POST)->accountsHistory();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Accounts Transaction | UMS</title>
    <?php include_once('../../includes/head.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once('../../includes/header.php');?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include_once('../../includes/sidebar.php');?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Accounts Summary
            </h1>
        </section>

        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> Accounts Transaction History
                        <small class="pull-right">Date: <?php date_default_timezone_set("Asia/Dhaka"); echo date('F d, Y');?></small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <address>
                        <strong>
                            <?php
                            if((array_key_exists('studentName',$_SESSION)&& (!empty($_SESSION['studentName'])))){
                                echo $_SESSION['studentName'];
                            }
                            ?>
                        </strong><br>
                        ID: <?php echo $_POST['studentID'];?><br>
                    </address>
                </div>

            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">

                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>BILL NO</th>
                            <th>TRN NO</th>
                            <th>DESCRIPTION</th>
                            <th>PAYABLE AMOUNT</th>
                            <th>AMOUNT PAID</th>
                            <th>AMOUNT DUE</th>
                            <th>PAYMENT DATE</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalPayableAmount=0;
                        $totalAmountPaid=0;
                        $totalAmountDue=0;
                        foreach($allData AS $data):
                        ?>
                            <tr>
                                <td><?php echo $data['bill_no'];?></td>
                                <td><?php echo $data['transection_no'];?></td>
                                <td><?php echo $data['transection_name'].", ".(($data['semester']==1) ? "Autumn" : (($data['semester']==2) ? "Spring" : "Summer"))."-".$data['year'];?></td>
                                <td><?php echo number_format($data['total_bill'],2);?></td>
                                <td><?php echo number_format($data['total_paid'],2);?></td>
                                <td><?php echo number_format($data['total_due'],2);?></td>
                                <td><?php echo $data['rcv_date'];?></td>
                            </tr>
                        <?php
                        $totalPayableAmount+=$data['total_bill'];
                        $totalAmountPaid+=$data['total_paid'];
                        $totalAmountDue+=$data['total_due'];
                        endforeach;
                        ?>

                        </tbody>
                    </table>
                </div>
                <!-- /.col -->

                <!-- /.col -->
                <div class="col-xs-5 pull-right">
                    <p class="lead"><strong>Summary</strong></p>

                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">Payable Amount</th>
                                <td><?php echo number_format($totalPayableAmount,2);?></td>
                            </tr>
                            <tr>
                                <th>Amount Paid</th>
                                <td><?php echo number_format($totalAmountPaid,2);?></td>
                            </tr>
                            <tr>
                                <th>Due:</th>
                                <td style="color:red;font-weight:bold;"><?php echo number_format($totalAmountDue,2);?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- this row will not appear when printing -->
            <div class="row no-print">
                <div class="col-xs-12">
                    <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                    <a href="pdf.php?id=<?php echo $_POST['studentID'];?>" target="_blank">
                        <i class="fa fa-download"></i> Generate PDF
                    </a>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('../../includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include_once('../../includes/script.php');?>
</body>
</html>