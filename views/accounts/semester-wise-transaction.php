<?php
include_once('../../vendor/autoload.php');
use App\Accounts\Accounts;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$loggedIn= $auth->prepare($_POST)->logged_in();//Check user activity status
if(!$loggedIn){
    $_SESSION['loggedInMessage']="<span style=\"color:red;\">You have to log in before enter this page</span>";
    return Utility::redirect('../../login.php');
}

if((array_key_exists('studentID',$_SESSION)&& (!empty($_SESSION['studentID'])))){
    $_POST['studentID']=$_SESSION['studentID'];
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Accounts Transaction | UMS</title>
    <?php include_once('../../includes/head.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php include_once('../../includes/header.php');?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include_once('../../includes/sidebar.php');?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Accounts Summary
            </h1>
        </section>

        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    <address>
                        <strong>
                            <?php
                            if((array_key_exists('studentName',$_SESSION)&& (!empty($_SESSION['studentName'])))){
                                echo $_SESSION['studentName'];
                            }
                            ?>
                        </strong><br>
                        ID: <?php echo $_POST['studentID'];?><br>
                    </address>
                </div>
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <?php
            $group = new Accounts();
            $group = $group->prepare($_POST)->searchSemesterAndYear();
            foreach($group AS $value):
            ?>
            <div class="row">
                <section class="content-header">
                    <h1>
                        <?php echo (($value['semester']==1) ? "Autumn" : (($value['semester']==2) ? "Spring" : "Summer"))."-".$value['year'];?>
                    </h1>
                </section>
                <div class="col-xs-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>BILL NO</th>
                            <th>TRN NO</th>
                            <th>DESCRIPTION</th>
                            <th>PAYABLE AMOUNT</th>
                            <th>AMOUNT PAID</th>
                            <th>AMOUNT DUE</th>
                            <th>PAYMENT DATE</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $_POST['semester']=$value['semester'];
                        $_POST['year']=$value['year'];
                        $allData = new Accounts();
                        $allData = $allData->prepare($_POST)->semesterWiseAccountsHistory();

                        $totalPayableAmount=0;
                        $totalAmountPaid=0;
                        $totalAmountDue=0;

                        foreach($allData AS $data):
                        ?>
                        <tr>
                            <td><?php echo $data['bill_no'];?></td>
                            <td><?php echo $data['transection_no'];?></td>
                            <td><?php echo $data['transection_name'].", ".(($data['semester']==1) ? "Autumn" : (($data['semester']==2) ? "Spring" : "Summer"))."-".$data['year'];?></td>
                            <td><?php echo number_format($data['total_bill'],2);?></td>
                            <td><?php echo number_format($data['total_paid'],2);?></td>
                            <td <?php if($data['total_due']>0) echo "style=\"color:red;font-weight:bold;\""?>><?php echo number_format($data['total_due'],2);?></td>
                            <td><?php echo $data['rcv_date'];?></td>
                        </tr>
                        <?php
                            $totalPayableAmount+=$data['total_bill'];
                            $totalAmountPaid+=$data['total_paid'];
                            $totalAmountDue+=$data['total_due'];
                        endforeach;
                        ?>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th><?php echo number_format($totalPayableAmount,2);?></th>
                        <th><?php echo number_format($totalAmountPaid,2);?></th>
                        <th <?php if($data['total_due']>0) echo "style=\"color:red;font-weight:bold;\""?>><?php echo number_format($totalAmountDue,2);?></th>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <?php
            endforeach;
            ?>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('../../includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include_once('../../includes/script.php');?>
</body>
</html>