<?php
include_once('../../vendor/autoload.php');

use App\evaluation\Evaluation;
use App\Routine\Routine;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth = new Auth();
$loggedIn = $auth->prepare($_POST)->logged_in(); //Check user activity status
if (!$loggedIn) {
    $_SESSION['loggedInMessage'] = "<span style=\"color:red;\">You have to log in before enter this page</span>";
    return Utility::redirect('../../login.php');
}

if ((array_key_exists('studentID', $_SESSION) && (!empty($_SESSION['studentID'])))) {
    $_POST['studentID'] = $_SESSION['studentID'];
}
$semester = $year = "";

$masterData = new Evaluation();
$allData = $masterData->getMasterData();
$semester = $_POST['semester'] = $allData['semester'];
$year = $_POST['year'] = $allData['year'];
//
//if (($_SERVER["REQUEST_METHOD"] == "POST") AND isset($_POST['search'])) {
//    if (isset($_POST['semester'])) {
//        $semester = $_POST['semester'];
//    }
//    if (isset($_POST['year'])) {
//        $year = $_POST['year'];
//    }
//}

$allData = new Evaluation();
$allData = $allData->prepare($_POST)->RoutineIndex();

$semesterTitle = "";
if ($semester==1) {
  $semesterTitle = "Autumn";
}
elseif ($semester==2) {
  $semesterTitle = "Spring";
}
elseif ($semester==3) {
  $semesterTitle = "Summer";
}

$evlActive = new Evaluation();
$evlActive = $evlActive->evlActivation();
//Utility::dd($evlActive);
$visibilityStatus = "";
  if ($evlActive['is_active']==0) {
    $visibilityStatus = 'invisible';
  }
  elseif($evlActive['is_active']==1) {
    $visibilityStatus = 'visible';
  }



?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Evaluation</title>
        <?php include_once('../../includes/head.php'); ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <?php include_once('../../includes/header.php'); ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include_once('../../includes/sidebar.php'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Evaluation
                    </h1>
                </section>

                <!-- Main content -->
                <section class="invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                                <i class="fa fa-etsy" aria-hidden="true"></i>
                                <p>
                                  Registered Courses for <?php echo $semesterTitle."-".$year; ?>
                                </p>
                                <small class="pull-right">Date: <?php date_default_timezone_set("Asia/Dhaka");
                                  echo date('F d, Y'); ?></small>
                            </h2>
                        </div>
                        <!-- /.col -->
                    </div>
<!--                    <form class="form-horizontal form-label-left" action="" method="post" novalidate>
                        <div class="item form-inline">
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <select id="heard" name="semester" class="form-control" required>
                                    <option value="">Semester</option>
                                    <option value="1">Autumn</option>
                                    <option value="2">Spring</option>
                                    <option value="3">Summer</option>
                                </select>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <select id="heard" name="year" class="form-control" required>
                                    <option value="">Year</option>
                                    <?php
//                                    $y = 0;
//                                    for ($y = 2013; $y <= 2030; $y++) {
//                                        echo'<option value=' . $y . '>' . $y . '</option>';
//                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button id="send" type="submit" class="btn btn-primary" name="search">Search</button>
                        </div>
                    </form>-->

                    <br/>
                    <br/>
                    <!-- info row -->
                    <!-- /.row -->
                    <!-- Table row -->
                    <div class="row">
                        <div class="col-xs-12 table-responsive">

                            <?php
                            if ($evlActive['is_active']==0) {

                            ?>
                            <h3 class="text-center bg-primary">
                              You can not evaluate now please check back again.
                            </h3>
                              <?php
                             }

                            ?>

                            <table class="table table-striped <?php echo $visibilityStatus;?>" >
                                <thead>
                                    <tr>
                                        <th class="text-center">#SL </th>
                                        <th class="text-center">Course ID</th>
                                        <th class="">Course Title</th>
                                        <th class="text-center">Section</th>
                                        <th class="text-center">Day</th>
                                        <th class="text-center">Time</th>
                                        <th class="">Instructor</th>
                                        <th class="text-center">Status of Evaluation</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $count=1;
                                    foreach ($allData AS $data):
                                        ?>
                                        <tr>
                                            <td class="text-center"><?php echo $count++ ; ?></td>
                                            <td class="text-center"><?php echo $data['course_code']; ?></td>
                                            <td class=""><?php echo $data['course_name']; ?></td>
                                            <td class="text-center"><?php echo $data['sec_id']; ?></td>
                                            <td class="text-center"><?php echo $data['day']; ?></td>
                                            <td class="text-center"><?php echo $data['time']; ?></td>
                                            <td class=""><?php echo $data['emp_name']; ?></td>
                                            <td class="text-center">
                                                <form action="evaluation-question.php" method="post">
                                                    <input type="hidden" name="courseID" value="<?php echo $data['course_code']; ?>">
                                                    <input type="hidden" name="sectionID" value="<?php echo $data['sec_id']; ?>">
                                                    <input type="hidden" name="employeeID" value="<?php echo $data['emp_code']; ?>">
                                                    <input type="hidden" name="semester" value="<?php echo $semester; ?>">
                                                    <input type="hidden" name="year" value="<?php echo $year; ?>">
                                                    <input type="hidden" name="courseIDor" value="<?php echo $data['course_id']; ?>">

                                                    <?php
                                                        if($data['evl_status']==0):
                                                        ?>
                                                  <input type="submit" class="btn btn-primary" name="evaluate" value="Evaluate"/>
                                                    <?php
                                                        elseif($data['evl_status']==1):
                                                            echo '<p class="text-primary">Evaluation Completed<p>';
                                                        endif;
                                                    ?>
                                                </form>

                                            </td>

                                        </tr>
                                        <?php
                                    endforeach;
                                    ?>

                                </tbody>
                            </table>

                        </div>
                        <!-- /.col -->

                        <!-- /.col -->

                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- this row will not appear when printing -->


                    <!-- /.content -->
            </div>

            <!-- /.content-wrapper -->
            <?php include_once('../../includes/footer.php'); ?>
        </div>
        <!-- ./wrapper -->
        <?php include_once('../../includes/script.php'); ?>

    </body>
</html>
