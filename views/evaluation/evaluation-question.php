<?php
include_once('../../vendor/autoload.php');

use App\evaluation\Evaluation;
use App\Routine\Routine;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth = new Auth();
$loggedIn = $auth->prepare($_POST)->logged_in(); //Check user activity status
if (!$loggedIn) {
    $_SESSION['loggedInMessage'] = "<span style=\"color:red;\">You have to log in before enter this page</span>";
    return Utility::redirect('../../login.php');
}

if ((array_key_exists('studentID', $_SESSION) && (!empty($_SESSION['studentID'])))) {
    $_POST['studentID'] = $_SESSION['studentID'];
}
$semester = $year = "";
if (($_SERVER["REQUEST_METHOD"] == "POST") AND isset($_POST['search'])) {
    if (isset($_POST['semester'])) {
        $semester = $_POST['semester'];
    }
    if (isset($_POST['year'])) {
        $year = $_POST['year'];
    }
}
//pUtility::dd($_POST);
//marks submission
if (isset($_POST['submitEvaluation'])) :
    $evlMarks = new Evaluation();
    $evlMarks = $evlMarks->prepare($_POST)->EvlMarksInsert($_POST);
endif;

//view question list
$allData = new Evaluation();
$programType = $allData->GetStudentInfo($_POST['studentID']);

$allData = $allData->prepare($_POST)->EvlQuestionList($programType['program_type']);

$tData = new Evaluation();
$facultyName = $tData->facultyName($_POST['employeeID']);

$program = "";
if ($programType['program_type']=="1") {
  $program = "Undergraduate";
}
elseif ($programType['program_type']=="2") {
  $program = "Graduate";

}
$semesterTitle = "";
if ($_POST['semester']==1) {
  $semesterTitle = "Autumn";
}
elseif ($_POST['semester']==2) {
  $semesterTitle = "Spring";
}
elseif ($_POST['semester']==3) {
  $semesterTitle = "Summer";
}

?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Evaluation Question</title>
        <?php include_once('../../includes/head.php'); ?>
        <style>
            input[type=radio] {
                height: 15px;
                width: 15px;
            }
            p{
                font-weight: bold;
                font-size: 15px;
            }
        </style>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <?php include_once('../../includes/header.php'); ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include_once('../../includes/sidebar.php'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Evaluation
                    </h1>
                    <p>
                    <?php echo $semesterTitle."-". $_POST['year']; ?>
                    </p>
                </section>

                <!-- Main content -->
                <section class="invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                                <i class="fa fa-etsy" aria-hidden="true"></i>Instruction
                            </h2>
                            <p>The student should read the instructions and the form carefully.Information to be provided in this form will be kept strictly confidential.You are completing teacher's evaluation for the following course. </p>
                            <p class="text-danger">
                              "Answering all questions are mandatory."
                            </p>

                        </div>

                        <!-- /.col -->
                    </div>

                    <div class="row">
                      <div class="col-xs-12">
                          <h4>
                              <i class="fa fa-etsy" aria-hidden="true"></i>Class Characteristics:
                          </h4>
                          <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>Program</th>
                                <th>Course ID</th>
                                <th>section</th>
                                <th>Name Of the instructor</th>
                              </tr>
                            </thead>
                            <tbody>
                            <tr>
                              <td><?php echo $program ;?></td>
                              <td><?php echo $_POST['courseID'];?></td>
                              <td><?php echo $_POST['sectionID'];?></td>
                              <td><?php echo $facultyName['emp_name'];?></td>
                             </tr>
                            </tbody>
                          </table>
                      </div>
                    </div>

                    <br/>
                    <br/>
                    <div class="row">
                        <div class="col-xs-12 table-responsive">
                            <form action="" method="POST">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#SL</th>
                                            <th>Questionary</th>
                                            <th style="text-align: center;" colspan="5">Points Distribution</th>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th style="text-align: center;">Always<br/><br/>[5]</th>
                                            <th style="text-align: center;">Almost<br/>Always<br/>[4]</th>
                                            <th style="text-align: center;">Most<br/>Times<br/>[3]</th>
                                            <th style="text-align: center;">Few<br/>Times<br/>[2]</th>
                                            <th style="text-align: center;">Almost<br/>Never<br/>[1]</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $i = 1;
                                        foreach ($allData AS $data):
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><p><?php echo $data['question']; ?></p></td>
                                                <td style="text-align: center;"><input type="radio" name="<?php echo $data['id']; ?>" value="5" required="true"></td>
                                                <td style="text-align: center;"><input type="radio" name="<?php echo $data['id']; ?>" value="4"></td>
                                                <td style="text-align: center;"><input type="radio" name="<?php echo $data['id']; ?>" value="3"></td>
                                                <td style="text-align: center;"><input type="radio" name="<?php echo $data['id']; ?>" value="2"></td>
                                                <td style="text-align: center;"><input type="radio" name="<?php echo $data['id']; ?>" value="1"></td>
                                            </tr>
                                            <?php
                                            $i++;
                                        endforeach;
                                        ?>
                                    </tbody>
                                </table>
                                <input type="submit" class="btn btn-success pull-right" name="submitEvaluation" value="Submit">
                                <input type="hidden" id="semester" name="semester" value="<?php echo $_POST['semester'];?>">
                                <input type="hidden" id="year" name="year" value="<?php echo $_POST['year'];?>">
                                <input type="hidden" id="courseCode" name="course_code" value="<?php echo $_POST['courseID'];?>">
                                <input type="hidden" id="courseCode" name="course_id" value="<?php echo $_POST['courseIDor'];?>">
                                <input type="hidden" id="secID" name="section" value="<?php echo $_POST['sectionID'];?>">
                                <input type="hidden" id="employeeID" name="employee_id" value="<?php echo $_POST['employeeID'];?>">
                                <input type="hidden" id="programType" name="program" value="<?php echo $programType['program_type'];?>">
                            </form>
                        </div>
                    </div>
            </div>
            <!-- /.content-wrapper -->
            <?php include_once('../../includes/footer.php'); ?>
        </div>
        <!-- ./wrapper -->
        <?php include_once('../../includes/script.php'); ?>
    </body>
</html>
