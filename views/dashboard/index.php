<?php
include_once('../../vendor/autoload.php');
use App\Profile\Profile;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$auth= new Auth();
$loggedIn= $auth->prepare($_POST)->logged_in();//Check user activity status
if(!$loggedIn){
    $_SESSION['loggedInMessage']="<span style=\"color:red;\">You have to log in before enter this page</span>";
    return Utility::redirect('../../login.php');
}

if((array_key_exists('studentID',$_SESSION)&& (!empty($_SESSION['studentID'])))){
    $_POST['studentID']=$_SESSION['studentID'];
}

$alldata=new Profile();
$data=$alldata->prepare($_POST)->index();

$_alldata = new Profile();
$allData=$_alldata->prepare($_POST)->studentEducation();
//var_dump($_data);die();

//Utility::dd($_SESSION);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My Profile | UMS</title>
    <?php include_once('../../includes/head.php');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php include_once('../../includes/header.php');?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php include_once('../../includes/sidebar.php');?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                My Profile
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-3">

                    <!-- Profile Image -->
                    <div class="box box-warning">
                        <div class="box-body box-profile">
                          <!--<img class="profile-user-img img-responsive img-circle" src="../../../ems-admin/images/profile-image/<?php if(isset($data['image']) AND !empty($data['image'])) echo $data['image']; else echo "1.gif";?>" alt="User profile picture">-->

                            <h3 class="profile-username text-center"><?php echo $data['student_name']; ?></h3>

                            <p class="text-muted text-center"><?php echo $data['school_name']; ?></p>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Student ID</b> <a class="pull-right"><?php echo $data['student_id']; ?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Major</b> <a class="pull-right"><?php if(isset($data['major']) AND !empty($data['major'])) echo $data['major']; else echo "Not Declared"?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Minor</b> <a class="pull-right"><?php if(isset($data['minor']) AND !empty($data['minor'])) echo $data['minor']; else echo "Not Declared"?></a>
                                </li>
                            </ul>

                            <!--<a href="#" class="btn btn-primary btn-block"><b>Follow Advisor</b></a>-->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                    <!-- About Me Box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">About Me</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <strong><i class="fa fa-map-marker margin-r-5"></i> Mailing Address</strong>

                            <p class="text-muted"><?php echo $data['mailing_address']; ?></p>

                            <hr>

                            <strong><i class="fa fa-map-marker margin-r-5"></i> Contact Number</strong>

                            <p class="text-muted"><?php echo $data['mobile']; ?></p>

                            <hr>

                            <strong><i class="fa fa-map-marker margin-r-5"></i> E-mail Address</strong>

                            <p class="text-muted"><?php echo $data['email']; ?></p>

                            <!--<strong><i class="fa fa-pencil margin-r-5"></i> Student Category</strong>

                            <p>
                                <span class="label label-default">Freshman</span>
                                <span class="label label-default">Sophomore</span>
                                <span class="label label-danger">Junior</span>
                                <span class="label label-default">Senior</span>
                            </p>-->

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#activity" data-toggle="tab">Personal</a></li>
                            <li><a href="#timeline" data-toggle="tab">Education</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="activity">
                                <div class="box">
                                    <!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table class="table">
                                            <tr>
                                                <td>Term to Begin</td>
                                                <td>
                                                    <?php if(isset($data['term_to_begin'])){
                                                        if($data['term_to_begin']==1){
                                                            echo "Autumn ";
                                                        }
                                                        else if($data['term_to_begin']==2){
                                                            echo "Spring ";
                                                        }
                                                        else{
                                                            echo "Summer ";
                                                        }
                                                    }
                                                    if(isset($data['year'])) echo $data['year'];?></td>
                                            </tr>
                                            <tr>
                                                <td>Program</td>
                                                <td><?php echo $data['category']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Intended Major</td>
                                                <td><?php echo $data['intended_major']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Gender</td>
                                                <td><?php echo $data['gender']; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                            <div class="tab-pane" id="timeline">
                                <div class="active tab-pane" id="activity">
                                    <div class="box">
                                        <!-- /.box-header -->
                                        <div class="box-body no-padding">
                                            <table class="table">
                                                <thead>
                                                <th>Degree</th>
                                                <th>Institution Name</th>
                                                <?php
                                                if(!empty($data['board'])){
                                                    ?>
                                                    <th>Board</th>
                                                <?php } ?>
                                                <th>Year</th>
                                                <th>Subject/Group</th>
                                                <th>GPA/Grade</th>
                                                </thead>
                                                <?php
                                                foreach($allData as $_data):
                                                ?>
                                                <tbody>
                                                <tr>
                                                    <td><?php echo $_data['degree']; ?></td>
                                                    <td><?php echo $_data['institution']; ?></td>
                                                    <?php
                                                    if(!empty($data['board'])){
                                                        ?>
                                                        <td><?php echo $_data['board']; ?></td>
                                                    <?php } ?>
                                                    <td><?php echo $_data['year']; ?></td>
                                                    <td><?php echo $_data['subject_group']; ?></td>
                                                    <td><?php echo $_data['grade']; ?></td>
                                                </tr>

                                                <?php
                                                endforeach;
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php include_once('../../includes/footer.php');?>
</div>
<!-- ./wrapper -->
<?php include_once('../../includes/script.php');?>
</body>
</html>
