<?php
include_once('vendor/autoload.php');

use App\Message\Message;
use App\Utility\Utility;

ob_clean();
if (session_id() == '') {
    session_start();
    ob_start();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CIU Student Panel</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="resource/admin/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="resource/admin/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="resource/admin/plugins/iCheck/square/blue.css">
        <!--tab icon-->
        <link rel="icon" href="resource/img/logo.png" type="image/gif" sizes="16x16">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .text-white{
                color: white;
            }
            .text-ciu-blue{
                color: #2e3092;
            }
            .opacityLevel{
                opacity: 0.6
            }
            .transparent-bg{
                background: transparent;
            }
            .custom-font-size{
                font-size: 14px;
            }
            footer{
                height: 4vh;
                bottom: 0;
                width: 100%;
                position: absolute;
            }
            .login-box, .register-box{
               margin: 4% auto;
               padding: 15px 0;
               background: #122e0066;
            }
        </style>
    </head>
    <body class="hold-transition login-page" style="background: url(resource/img/bg-1.jpeg) no-repeat center center fixed; background-size: cover;">
        <div class="login-box">
            <div class="login-logo">                
                <img width="120" height="120" src="resource/img/logo.png" alt="ciu_logo"/>
                <p style="height: 5px;"></p>
                <!--<p class="bg-primary opacityLevel">CIU Student Panel</p>-->
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body transparent-bg">

                <?php
                if ((array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))) {
                    ?>
                    <div class="login-box-msg" id="message">
                        <?php echo Message::message(); ?>
                    </div>
                    <?php
                }
                ?>


                <form action="views/authetication/login.php" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" name="email" class="form-control" placeholder="Student ID" required autocomplete="off">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="pass" class="form-control" placeholder="Password" required autocomplete="off">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">                        
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                        <div class="col-xs-4"></div>
                        <!-- /.col -->
                    </div>
                </form>

                <!-- /.social-auth-links -->

                <!--<a href="#">I forgot my password</a><br>-->
                <br/>
                <div class="custom-font-size">
                    <span class="text-white">Don't have an account?</span> <a href="views/signup/sign-up.php" class="text-center text-bold bg-gray">Sign up/Register</a>
                </div>
            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->
        <footer class="transparent-bg text-white text-center">
            <small> &copy; 2018-<?php echo date("Y"); ?> <strong>CIU SOFTWARE TEAM</strong> All Rights Reserved </small>
                <!--<div class="pull-right hidden-xs">
                    Developed by<strong> CIU SOFTWARE TEAM </strong>
                </div>
                <strong>University Management System</strong> reserved.-->
        </footer>

        <!-- jQuery 2.2.3 -->
        <script src="resource/admin/plugins/jQuery/jquery-2.2.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="resource/admin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="resource/admin/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
        <script>
            var x = document.getElementById("message").innerHTML;
            if (x) {
                $('#message').show().delay(3000).fadeOut();
            }
//            $('#message').show().delay(3000).fadeOut();
        </script>
    </body>
</html>