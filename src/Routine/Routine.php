<?php
namespace App\Routine;
use App\Message\Message;
use App\Utility\Utility;
include_once('../../vendor/autoload.php');

use App\Model\Database as DB;

class Routine extends DB
{
    public $id = NULL;
    public $studentID =NULL;
    public $studentName = NULL;
    public $schoolname = NULL;
    public $category = NULL;
    public $termToBegin = NULL;
    public $maillingAdress = NULL;
    public $mobile = NULL;
    public $email = NULL;
    public $gender = NULL;
    public $maritialStatus = NULL;
    public $dateOfBirth = NULL;
    public $institutionName = NULL;
    public $board = NULL;
    public $year = NULL;
    public $subjectGroup = NULL;
    public $grade = NULL;
    public $semester = NULL;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data=array()){

        if(array_key_exists("studentID",$data)){
            $data['studentID']=Utility::validation($data['studentID']);
            $this->studentID=filter_var($data['studentID'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if(array_key_exists("semester",$data)){
            $data['semester']=Utility::validation($data['semester']);
            $this->semester=filter_var($data['semester'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if(array_key_exists("year",$data)){
            $data['year']=Utility::validation($data['year']);
            $this->year=filter_var($data['year'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        return $this;
    }

    public function RoutineIndex(){

        $_allData = array();
        $query="SELECT
                  `adv_course_advising`.`student_id`,
                  `cat_course_list`.`course_code`,
                  `cat_course_list`.`course_name`,
                  `reg_routine_setup`.`sec_id`,
                  `reg_routine_setup`.`day`,
                  `reg_routine_setup`.`time`,
                  `hr_employee_personal_info`.`emp_name`,
                  `hr_employee_personal_info`.`emp_code`,
                  `ems_room_setup`.`room_no`,
                  `ems_room_setup`.`room_name`
                FROM
                  `adv_course_advising`
                  INNER JOIN `reg_routine_setup` ON `adv_course_advising`.`routine_id` =
                    `reg_routine_setup`.`id`
                  INNER JOIN `cat_course_list` ON `reg_routine_setup`.`course_id` =
                    `cat_course_list`.`course_id`
                  INNER JOIN `hr_employee_personal_info` ON `reg_routine_setup`.`employee_id` =
                    `hr_employee_personal_info`.`emp_code`
                  INNER JOIN `ems_room_setup` ON `reg_routine_setup`.`room` =
                    `ems_room_setup`.`id`
                WHERE
                  `adv_course_advising`.`student_id` = '$this->studentID' AND `adv_course_advising`.`semester`='$this->semester' AND `adv_course_advising`.`year`='$this->year' AND
                  (`adv_course_advising`.`course_status`=1 OR  `adv_course_advising`.`course_status`=2)";

        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;
    }


}