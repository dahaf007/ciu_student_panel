<?php
namespace App\Dashboard;
use App\Message\Message;
use App\Utility\Utility;
include_once('../../vendor/autoload.php');

use App\Model\Database as DB;

class Dashboard extends DB
{
    public $id = NULL;
    public $employeeID=NULL;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data=array()){
        if(array_key_exists("employeeID",$data)){
            $data['employeeID']=Utility::validation($data['employeeID']);

            $this->employeeID=filter_var($data['employeeID'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }

        if(array_key_exists("id",$data)){
            $data['id']=Utility::validation($data['id']);
            $this->id=filter_var($data['id'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        return $this;
    }

    public function meetingListForSupervisor(){

        $_allData = array();
        $query="SELECT * FROM `booking` WHERE `booking`.`employee_id`='".$this->id."' AND `booking`.`is_delete` IS NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }

        return $_allData;
    }

    public function meetingListForMember(){

        $_allData = array();
        $query="SELECT
                  `booking`.`meeting_title`,
                  `room`.`room_name`,
                  `room`.`room_no`,
                  `booking`.`start_date`,
                  `booking`.`start_time`,
                  `booking`.`end_date`,
                  `booking`.`end_time`,
                  `user`.`employee_id`,
                  `designation`.`designation_name`,
                  `department`.`department_name`,
                  `meeting_invitee`.`is_attend`
                FROM
                  `meeting_invitee`
                  INNER JOIN `meeting`
                    ON `meeting`.`meeting_id` = `meeting_invitee`.`meeting_id`
                  INNER JOIN `booking` ON `booking`.`booking_id` = `meeting`.`booking_id`
                  INNER JOIN `room` ON `room`.`room_id` = `booking`.`room_id`
                  INNER JOIN `user` ON `user`.`employee_id` = `booking`.`employee_id`
                  INNER JOIN `designation` ON `designation`.`designation_id` =
                    `user`.`designation_id`
                  INNER JOIN `department` ON `department`.`department_id` =
                    `user`.`department_id`
                WHERE `meeting_invitee`.`employee_id`='".$this->id."' AND `meeting_invitee`.`is_attend`='Yes'";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }

        return $_allData;
    }

    public function meetingListForSuperAdmin(){

        $_allData = array();
        $query="SELECT
                  `booking`.`meeting_title`,
                  `room`.`room_name`,
                  `room`.`room_no`,
                  `booking`.`start_date`,
                  `booking`.`start_time`,
                  `booking`.`end_date`,
                  `booking`.`end_time`,
                  `booking`.`is_approved`,
                  `user`.`employee_id`,
                  `designation`.`designation_name`,
                  `department`.`department_name`
                FROM
                  `meeting`
                  RIGHT JOIN `booking` ON `booking`.`booking_id` = `meeting`.`booking_id`
                  INNER JOIN `room` ON `room`.`room_id` = `booking`.`room_id`
                  INNER JOIN `user` ON `user`.`employee_id` = `booking`.`employee_id`
                  INNER JOIN `designation` ON `designation`.`designation_id` =
                    `user`.`designation_id`
                  INNER JOIN `department` ON `department`.`department_id` =
                    `user`.`department_id`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }

        return $_allData;
    }

    public function reject(){
        $query="UPDATE `booking` SET `is_approved` = NULL WHERE `booking`.`booking_id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result>0){
            Message::message(
                "
                <div class=\"alert alert-success\" role=\"alert\">
                    <strong>Success!</strong> Data has been booked successfully.
                </div>
                ");
            Utility::redirect('index.php');
        }
        else{
            Message::message(
                "
                   <div class=\"alert alert-danger\" role=\"alert\">
                    <strong>Error!</strong> Data has not been booked successfully.
                   </div>
                ");
            Utility::redirect('index.php');
        }
    }

    public function approve(){
        $query="UPDATE `booking` SET `is_approved` = 'Yes' WHERE `booking`.`booking_id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result>0){
            Message::message(
                "
                <div class=\"alert alert-success\" role=\"alert\">
                    <strong>Success!</strong> Data has been approved successfully.
                </div>
                ");
            Utility::redirect('index.php');
        }
        else{
            Message::message(
                "
                   <div class=\"alert alert-danger\" role=\"alert\">
                    <strong>Error!</strong> Data has not been approved successfully.
                   </div>
                ");
            Utility::redirect('index.php');
        }
    }

    public function latestMembers(){
        $query="SELECT * FROM (
                    SELECT * FROM user ORDER BY id DESC LIMIT 8
                ) sub
                ORDER BY created_date ASC";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;

    }

    public function numberOfUpcomingMeeting(){


        $query="SELECT COUNT(*) AS totalUpcomingMeeting
                FROM
                  `booking`
                  INNER JOIN `meeting` ON `booking`.`booking_id` = `meeting`.`booking_id`
                  INNER JOIN `meeting_invitee` ON `meeting`.`meeting_id` =
                    `meeting_invitee`.`meeting_id`
                WHERE
                  `meeting_invitee`.`employee_id` = '".$_POST['id']."' AND
                  `booking`.`start_date` > Now() AND
                  `booking`.`is_approved` = 'Yes'";

        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function meetingCompleted(){
        $query="SELECT COUNT(*) AS totalCompleted FROM booking WHERE `end_date` < NOW() AND `is_approved`='Yes'";
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function totalUsers(){
        $query="SELECT COUNT(*) AS totalUser FROM `user` WHERE `is_inactive` IS NULL AND `is_delete` IS NULL";
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function totalRoom(){
        $query="SELECT COUNT(*) AS totalRoom FROM `room` WHERE `is_inactive` IS NULL AND `is_delete` IS NULL";
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function searchRoomForCalendar(){
        $query="SELECT `room_id`, `room_name` FROM `room`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;
    }

    public function searchBookingForCalendarTable(){
        $_allData=array();
        $query="SELECT * FROM `booking` WHERE `booking`.`start_date`>=NOW() AND `booking`.`is_approved`='Yes' AND `is_delete` IS NULL AND `booking`.`room_id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }

        return $_allData;
    }

}