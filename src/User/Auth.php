<?php
namespace App\User;
session_start();
use App\Model\Database as DB;
use App\Utility\Utility;

class Auth extends DB
{   
    public $email = NULL;
    public $pass = NULL;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data = Array())
    {
        if(array_key_exists("email",$data)){
            $data['email']=Utility::validation($data['email']);
            $this->email=filter_var($data['email'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if(array_key_exists("pass",$data)){
            $secretCode = 'le@F06';
            $data['pass']=Utility::validation($data['pass']);
            $data['pass']=filter_var($data['pass'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $passWithSecretCode = $data['pass'].$secretCode;
            $this->pass=md5($passWithSecretCode);
        }
        return $this;
    }

    /* public function is_exist()
    {
        $query = "SELECT * FROM `users` WHERE `email`='" . $this->email . "'";

        $result = mysqli_query($this->conn, $query);
        //$row= mysqli_fetch_assoc($result);
        if (mysqli_num_rows($result) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }

    } */

    public function is_registered()
    {        
        $query = "SELECT * FROM `bm_student` WHERE `userEmail`='" . $this->email . "' AND `userPass`='" . $this->pass . "' AND `roleID`=5050";
        $result = mysqli_query($this->conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function is_active()
    {
        $query = "SELECT * FROM `bm_student` WHERE `userEmail`='" . $this->email . "' AND `userPass`='" . $this->pass . "' AND `isInactive` IS NULL";
        $result = mysqli_query($this->conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function logged_in()
    {        
	if ((array_key_exists('studentID', $_SESSION)) && (!empty($_SESSION['studentID']))) {                    
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function log_out(){
        session_destroy();
        return TRUE;
    }

    public function loggedInDetails()
    {
        $updateQuery = "UPDATE `bm_student` SET `userLastVisitDate`= NOW() WHERE `studentID` = ".$this->email;
        $updateResult = mysqli_query($this->conn,$updateQuery);
        
        $query = "SELECT * FROM `ems_student_info` WHERE `student_id`='" . $this->email . "'";
        $result = mysqli_query($this->conn,$query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }
}

