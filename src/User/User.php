<?php
namespace App\User;
use App\Message\Message;
use App\Utility\Utility;
include_once('../../vendor/autoload.php');
use App\Model\Database as DB;

class User extends DB
{
    public $userName = NULL;
    public $studentID = NULL;
    public $userPass = NULL;
    public $roleID = 5050;
    public $id = NULL;
    //public $password = NULL;
    //public $employeeID = NULL;
   // public $image = NULL;

    public function __construct()
    {
        parent::__construct();
	//$this->conn = mysqli_connect("localhost", "ems_admin", "5Xr^A*XC", "ems_dbdatabox") or die("Connection failed");
    }
    public function prepare($data=array()){

        if(array_key_exists("userName",$data)){
            $data['userName']=Utility::validation($data['userName']);
            $this->userName=filter_var($data['userName'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if(array_key_exists("userPass",$data)){
            $data['userPass']=Utility::validation($data['userPass']);
            $this->userPass=filter_var($data['userPass'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
	if(array_key_exists("studentID",$data)){
            $data['studentID']=Utility::validation($data['studentID']);
            $this->studentID=filter_var($data['studentID'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if(array_key_exists("roleID",$data)){
            $data['roleID']=Utility::validation($data['roleID']);
            $this->studentID=filter_var($data['roleID'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        /* if(array_key_exists("password",$data)){
            $data['password']=Utility::validation($data['password']);
            $data['password']=filter_var($data['password'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $this->password=md5($data['password']);
        } */

        /* if(array_key_exists("image",$data)){
            $data['image']=Utility::validation($data['image']);
            $this->image=filter_var($data['image'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        } */

        if(array_key_exists("id",$data)){
            $data['id']=Utility::validation($data['id']);
            $this->id=filter_var($data['id'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }

        return $this;
    }

    public function index(){
        $_allData = array();
        $query="SELECT
                  `user`.*,
                  `department`.`department_name`,
                  `designation`.`designation_name`,
                  `role`.`role_name`
                FROM
                  `user`
                  INNER JOIN `designation` ON `user`.`designation_id` =
                    `designation`.`designation_id`
                  INNER JOIN `department` ON `user`.`department_id` =
                    `department`.`department_id`
                  INNER JOIN `role` ON `role`.`role_id` = `user`.`role_id`
                WHERE
                  `user`.`is_delete` IS NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;
    }

    public function is_exist()	
    {        
        $query = "SELECT * FROM `bm_student` WHERE `UserEmail`='" . $this->studentID . "'";
        $result = mysqli_query($this->conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function is_valid_student_id(){
        $query = "SELECT `student_id` FROM `ems_student_info` WHERE `student_id` = '" . $this->studentID . "'";
        $result = mysqli_query($this->conn, $query);
        if (mysqli_num_rows($result) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function store(){
        $stdID = "SELECT `student_name` FROM `ems_student_info` WHERE `student_id` = '" . $this->studentID . "'"; //getting student name via student ID
        $stdResult = mysqli_query($this->conn, $stdID);
        $stdRow = mysqli_fetch_assoc($stdResult);
        $this->userName = $stdRow['student_name'];
        
        $secretCode = 'le@F06';
        $passWithSecretCode = $this->userPass.$secretCode;
        $query="INSERT INTO `bm_student` (`userName`,`studentID`, `userPass`, `userEmail`, `userRegDate`, `userLastVisitDate`, `roleID`) VALUES ('".$this->userName."', '".$this->studentID."',MD5('".$passWithSecretCode."'), '".$this->studentID."', NOW(), NOW(), '".$this->roleID."')";
	$result=mysqli_query($this->conn,$query);
        return $result;
    }

    public function view(){

        $query="SELECT
                  `user`.*,
                  `department`.`department_name`,
                  `designation`.`designation_name`,
                  `role`.`role_name`
                FROM
                  `user`
                  INNER JOIN `designation` ON `user`.`designation_id` =
                    `designation`.`designation_id`
                  INNER JOIN `department` ON `user`.`department_id` =
                    `department`.`department_id`
                  INNER JOIN `role` ON `role`.`role_id` = `user`.`role_id`
                WHERE `user`.`employee_id`=".$this->id;

        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function update(){
        if($this->password==md5("")){
            $query="UPDATE `user` SET `first_name` = '".$this->firstName."', `last_name` = '".$this->lastName."', `department_id` = '".$this->departmentID."', `designation_id` = '".$this->designationID."', `image` = '".$this->image."', `last_modified_date` = NOW() WHERE `user`.`employee_id` =".$this->id;
        }
        else{
            $query="UPDATE `user` SET `first_name` = '".$this->firstName."', `last_name` = '".$this->lastName."', `department_id` = '".$this->departmentID."', `designation_id` = '".$this->designationID."', `image` = '".$this->image."', `password`='".$this->password."',`last_modified_date` = NOW() WHERE `user`.`employee_id` =".$this->id;
        }
        $result=mysqli_query($this->conn,$query);
        if($result>0){
            Message::message(
                "
                <div class=\"alert alert-success\" role=\"alert\">
                    <strong>Success!</strong> Data has been updated successfully.
                </div>
                ");
            Utility::redirect('index.php');
        }
        else{
            Message::message(
                "
                   <div class=\"alert alert-danger\" role=\"alert\">
                    <strong>Error!</strong> Data has not been updated successfully.
                   </div>
                ");
            Utility::redirect('index.php');
        }
    }

    public function trash(){
        $query="UPDATE `user` SET `is_delete` = 'Yes' WHERE `user`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result>0){
            Message::message(
                "
                <div class=\"alert alert-success\" role=\"alert\">
                    <strong>Success!</strong> Data has been trashed successfully.
                </div>
                ");
            Utility::redirect('index.php');
        }
        else{
            Message::message(
                "
                   <div class=\"alert alert-danger\" role=\"alert\">
                    <strong>Error!</strong> Data has not been trashed successfully.
                   </div>
                ");
            Utility::redirect('index.php');
        }
    }
    public function active(){
        $query="UPDATE `user` SET `is_inactive` = NULL WHERE `user`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result>0){
            Message::message(
                "
                <div class=\"alert alert-success\" role=\"alert\">
                    <strong>Success!</strong> Data has been activated successfully.
                </div>
                ");
            Utility::redirect('index.php');
        }
        else{
            Message::message(
                "
                   <div class=\"alert alert-danger\" role=\"alert\">
                    <strong>Error!</strong> Data has not been activated successfully.
                   </div>
                ");
            Utility::redirect('index.php');
        }
    }

    public function inactive(){
        $query="UPDATE `user` SET `is_inactive` = 'Yes' WHERE `user`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result>0){
            Message::message(
                "
                <div class=\"alert alert-success\" role=\"alert\">
                    <strong>Success!</strong> Data has been inactive successfully.
                </div>
                ");
            Utility::redirect('index.php');
        }
        else{
            Message::message(
                "
                   <div class=\"alert alert-danger\" role=\"alert\">
                    <strong>Error!</strong> Data has not been inactive successfully.
                   </div>
                ");
            Utility::redirect('index.php');
        }
    }
}