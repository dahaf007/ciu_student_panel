<?php

namespace App\Evaluation;

use App\Message\Message;
use App\Utility\Utility;

include_once('../../vendor/autoload.php');

use App\Model\Database as DB;

class Evaluation extends DB {

    public $id = NULL;
    public $studentID = NULL;
    public $studentName = NULL;
    public $category = NULL;
    public $year = NULL;
    public $semester = NULL;
    public $courseCode = NULL;
    Public $secID = NULL;
    Public $employeeID = NULL;
    Public $questionID = NULL;
    Public $marks = NULL;
    Public $average = NULL;
    Public $totalStudents = NULL;
    Public $program = NULL;
    public $courseID =NULL;
    public $evlActive = NULL;

    public function __construct() {
        parent::__construct();
    }

    public function prepare($data = array()) {

        if (array_key_exists("studentID", $data)) {
            $data['studentID'] = Utility::validation($data['studentID']);
            $this->studentID = filter_var($data['studentID'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("semester", $data)) {
            $data['semester'] = Utility::validation($data['semester']);
            $this->semester = filter_var($data['semester'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("year", $data)) {
            $data['year'] = Utility::validation($data['year']);
            $this->year = filter_var($data['year'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("course_code", $data)) {
            $data['course_code'] = Utility::validation($data['course_code']);
            $this->courseCode = filter_var($data['course_code'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("course_id", $data)) {
            $data['course_id'] = Utility::validation($data['course_id']);
            $this->courseID = filter_var($data['course_id'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("section", $data)) {
            $data['section'] = Utility::validation($data['section']);
            $this->secID = filter_var($data['section'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("employee_id", $data)) {
            $data['employee_id'] = Utility::validation($data['employee_id']);
            $this->employeeID = filter_var($data['employee_id'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("questionID", $data)) {
            $data['questionID'] = Utility::validation($data['questionID']);
            $this->questionID = filter_var($data['year'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("marks", $data)) {
            $data['marks'] = Utility::validation($data['marks']);
            $this->marks = filter_var($data['marks'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("average", $data)) {
            $data['average'] = Utility::validation($data['average']);
            $this->average = filter_var($data['average'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("totalStudents", $data)) {
            $data['totalStudents'] = Utility::validation($data['totalStudents']);
            $this->totalStudents = filter_var($data['totalStudents'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if (array_key_exists("program", $data)) {
            $data['program'] = Utility::validation($data['program']);
            $this->program = filter_var($data['program'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        return $this;
    }

    public function RoutineIndex() {

        $_allData = array();
        $query = "SELECT
                  `adv_course_advising`.`student_id`,
                  `cat_course_list`.`course_code`,
                  `cat_course_list`.`course_id`,
                  `cat_course_list`.`course_name`,
                  `reg_routine_setup`.`sec_id`,
                  `reg_routine_setup`.`day`,
                  `reg_routine_setup`.`time`,
                  `hr_employee_personal_info`.`emp_name`,
                  `hr_employee_personal_info`.`emp_code`,
                  `ems_room_setup`.`room_no`,
                  `ems_room_setup`.`room_name`,
                  `adv_course_advising`.`evl_status`
                FROM
                  `adv_course_advising`
                  INNER JOIN `reg_routine_setup` ON `adv_course_advising`.`routine_id` =
                    `reg_routine_setup`.`id`
                  INNER JOIN `cat_course_list` ON `reg_routine_setup`.`course_id` =
                    `cat_course_list`.`course_id`
                  INNER JOIN `hr_employee_personal_info` ON `reg_routine_setup`.`employee_id` =
                    `hr_employee_personal_info`.`emp_code`
                  INNER JOIN `ems_room_setup` ON `reg_routine_setup`.`room` =
                    `ems_room_setup`.`id`
                WHERE
                  `adv_course_advising`.`student_id` = '$this->studentID' AND `adv_course_advising`.`semester`='$this->semester' AND `adv_course_advising`.`year`='$this->year' AND
                  (`adv_course_advising`.`course_status`=1 OR  `adv_course_advising`.`course_status`=2)";

        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allData[] = $row;
        }
        return $_allData;
    }

    public function EvlQuestionList($sentProgramType) {

        $query = "SELECT `e_question_bank`.`id`,
                `e_question_bank`.`question`
                FROM `e_question_bank` INNER JOIN `e_question_set`
                ON `e_question_set`.`questionId` = `e_question_bank`.`id`
                WHERE (`e_question_set`.`programType` = $sentProgramType or `e_question_set`.`programType` = 3)
                AND `e_question_set`.`semester` = '" . $this->semester . "' AND `e_question_set`.`year` = '" . $this->year . "'";

        $result = mysqli_query($this->conn, $query);
        return $result;
    }

    public function EvlMarksInsert($allData) {

        $myArray = $allData;
        $questionArray = array();
        $marksArray = array();

        foreach ($myArray as $key => $value) {
            if (is_numeric($key)) {
                $questionArray[] = $key;
                $marksArray[] = intval($value);
            }
        }
        $questionString = implode(",", $questionArray);
        $marksString = implode(",", $marksArray);
               $query = "INSERT INTO `evl_marks`( `student_id`,`semester`, `year`, `program`, `course_code`, `section`, `employee_id`, `question_id`, `marks`)
               VALUES ('".$this->studentID."','" . $this->semester . "','" . $this->year . "','" . $this->program . "','" . $this->courseCode . "','" . $this->secID . "','" . $this->employeeID . "','" . $questionString . "','" . $marksString . "')";
                //Utility::dd($query);
               $result = mysqli_query($this->conn, $query);

               $sql = "UPDATE `adv_course_advising` SET `evl_status`= '1' WHERE `student_id` = '".$this->studentID."' AND `course_id`= '".$this->courseID."' AND `semester` = '".$this->semester."' AND `year` = '" . $this->year . "' AND `section`='" . $this->secID . "'";
               $result2 = mysqli_query($this->conn, $sql);
              // return $result;
               Utility::redirect('index.php');
    }

    public function GetStudentInfo($studentID) {
        $query = "SELECT `ems_student_info`.`program_type` FROM `ems_student_info` WHERE `student_id` = $studentID";
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            $row = mysqli_fetch_assoc($result);
        }
        return $row;
    }

    public function getMasterData()
    {
        $query1 = "SELECT `master_value` FROM `ems_master_settings` WHERE `master_name`= 'current_semester'";
        $result1 = mysqli_query($this->conn, $query1);
        $row1 = mysqli_fetch_assoc($result1);
        $query2= "SELECT `master_value` FROM `ems_master_settings` WHERE `master_name`= 'current_year'";
        $result2 = mysqli_query($this->conn, $query2);
        $row2 = mysqli_fetch_assoc($result2);
        $data = array('semester' => $row1['master_value'], 'year' => $row2['master_value']);
        return $data;

    }
    public function evlActivation()
    {
      $query = "SELECT `evl_active`.`is_active` FROM `evl_active` WHERE `id` = 1";
      $result = mysqli_query($this->conn, $query);
      if ($result) {
          $row = mysqli_fetch_assoc($result);
      }
      return $row;

    }

     public function facultyName($employeeID)
    {
      $query = "SELECT `hr_employee_official_info`.`emp_name` FROM `hr_employee_official_info` WHERE `hr_employee_official_info`.`emp_code` = $employeeID ";

      $result = mysqli_query($this->conn, $query);
      if ($result) {
          $row = mysqli_fetch_assoc($result);
      }
      return $row;
    }
}
