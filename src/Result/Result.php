<?php
namespace App\Result;
use App\Message\Message;
use App\Utility\Utility;
include_once('../../vendor/autoload.php');

use App\Model\Database as DB;

class Result extends DB
{

    public $studentID =NULL;
    public $semester =NULL;
    public $year =NULL;
    public $semesterTitle =NULL;
    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data=array()){
        if(array_key_exists("studentID",$data)){
            $data['studentID']=Utility::validation($data['studentID']);
            $this->studentID=filter_var($data['studentID'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }

        if(array_key_exists("semester",$data)){
            $data['semester']=Utility::validation($data['semester']);
            $this->semester=filter_var($data['semester'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }

        if(array_key_exists("year",$data)){
            $data['year']=Utility::validation($data['year']);
            $this->year=filter_var($data['year'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }

        if(array_key_exists("semesterTitle",$data)){
            $data['semesterTitle']=Utility::validation($data['semesterTitle']);
            $this->semesterTitle=filter_var($data['semesterTitle'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }

        return $this;
    }

    public function semesterTitle(){
        $_allData = array();
        $query="SELECT DISTINCT `grd_transcript`.`semester_title` FROM `grd_transcript` WHERE `student_id` = '$this->studentID' ";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;
    }

    public function semesterResult(){
        $query="SELECT DISTINCT `grd_transcript`.`semester_title` FROM `grd_transcript` WHERE `student_id` = '$this->studentID' ";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;
    }

    public function semesterTitleWiseTranscript(){
        $query = "SELECT
                      `grd_transcript`.*,
                      `cat_course_list`.`course_code`,
                      `cat_course_list`.`course_name`
                    FROM
                      `grd_transcript`
                      INNER JOIN `cat_course_list` ON `grd_transcript`.`course_id` =
                        `cat_course_list`.`course_id`
                    WHERE student_id='$this->studentID' AND semester_title='$this->semesterTitle'";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;
    }
}
