<?php
namespace App\Profile;
use App\Message\Message;
use App\Utility\Utility;
include_once('../../vendor/autoload.php');

use App\Model\Database as DB;

class Profile extends DB
{
    public $id = NULL;
    public $studentID =NULL;
    public $studentName = NULL;
    public $schoolname = NULL;
    public $category = NULL;
    public $termToBegin = NULL;
    public $maillingAdress = NULL;
    public $mobile = NULL;
    public $email = NULL;
    public $gender = NULL;
    public $maritialStatus = NULL;
    public $dateOfBirth = NULL;
    public $institutionName = NULL;
    public $board = NULL;
    public $year = NULL;
    public $subjectGroup = NULL;
    public $grade = NULL;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data=array()){
        if(array_key_exists("studentID",$data)){
            $data['studentID']=Utility::validation($data['studentID']);
            $this->studentID=filter_var($data['studentID'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        return $this;
    }

    public function index(){
        $query="SELECT
                  `ems_student_info`.`image`,
                  `ems_student_info`.`student_id`,
                  `ems_student_info`.`student_name`,
                  `ems_school_setup`.`school_name`,
                  `ems_program_type`.`category`,
                  `ems_student_info`.`term_to_begin`,
                  `ems_student_info`.`mailing_address`,
                  `ems_student_info`.`mobile`,
                  `ems_student_info`.`email`,
                  `ems_student_info`.`gender`,
                  `ems_student_info`.`marital_status`,
                  `ems_student_info`.`date_of_birth`,
                  `ems_student_info`.`year`,
                  `ems_major_setup`.`name` AS `intended_major`,
                  `ems_major_setup1`.`name` AS `major`,
                  `ems_major_setup2`.`name` AS `minor`,
                  `cat_catalogue_setup`.`catalogue_name`
                FROM
                  `ems_student_info`
                  INNER JOIN `ems_school_setup` ON `ems_student_info`.`school_id` =
                    `ems_school_setup`.`school_id`
                  INNER JOIN `ems_program_type` ON `ems_student_info`.`program_type` =
                    `ems_program_type`.`id`
                  INNER JOIN `ems_major_setup` ON `ems_student_info`.`intended_major` =
                    `ems_major_setup`.`id`
                  LEFT JOIN `reg_major_minor_declared` ON `ems_student_info`.`student_id` =
                    `reg_major_minor_declared`.`student_id`
                  LEFT JOIN `ems_major_setup` `ems_major_setup1`
                    ON `reg_major_minor_declared`.`major_id` = `ems_major_setup1`.`id`
                  LEFT JOIN `ems_major_setup` `ems_major_setup2`
                    ON `reg_major_minor_declared`.`minor_id` = `ems_major_setup2`.`id`
                  LEFT JOIN `cat_catalogue_setup` ON `reg_major_minor_declared`.`catalogue_id`
                    = `cat_catalogue_setup`.`catalogue_id`
                WHERE
                  `ems_student_info`.`student_id` = '$this->studentID'";
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function studentEducation (){

        $_allData = array();
        $query="SELECT
                  `ems_student_qualification`.`degree`,
                  `ems_student_qualification`.`institution`,
                  `ems_student_qualification`.`board`,
                  `ems_student_qualification`.`year`,
                  `ems_student_qualification`.`subject_group`,
                  `ems_student_qualification`.`grade`,
                  `ems_student_qualification`.`student_id`
                FROM
                  `ems_student_qualification`
                 WHERE `ems_student_qualification`.`student_id` = '$this->studentID'";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;
    }
}