<?php
namespace App\Accounts;
use App\Message\Message;
use App\Utility\Utility;
include_once('../../vendor/autoload.php');

use App\Model\Database as DB;

class Accounts extends DB
{

    public $studentID =NULL;
    public $semester =NULL;
    public $year =NULL;
    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data=array()){
        if(array_key_exists("studentID",$data)){
            $data['studentID']=Utility::validation($data['studentID']);
            $this->studentID=filter_var($data['studentID'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }

        if(array_key_exists("semester",$data)){
            $data['semester']=Utility::validation($data['semester']);
            $this->semester=filter_var($data['semester'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }

        if(array_key_exists("year",$data)){
            $data['year']=Utility::validation($data['year']);
            $this->year=filter_var($data['year'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }

        return $this;
    }

    public function accountsHistory(){
        $query="SELECT
                  `ems_student_bill`.*,
                  `acc_transection_type`.`transection_type` AS `transection_name`
                FROM
                  `acc_transection_type`
                  INNER JOIN `ems_student_bill` ON `acc_transection_type`.`transection_type_id`
                    = `ems_student_bill`.`transection_type`
                WHERE
                  `ems_student_bill`.`student_id` = '$this->studentID'";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;
    }

    public  function searchSemesterAndYear(){
        $query="SELECT distinct(`semester`),`year` FROM `ems_student_bill` WHERE `student_id`='$this->studentID' ORDER BY `id`";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;
    }
    public function semesterWiseAccountsHistory(){
        $_allData=array();
        $query="SELECT
                  `ems_student_bill`.*,
                  `acc_transection_type`.`transection_type` AS `transection_name`
                FROM
                  `acc_transection_type`
                  INNER JOIN `ems_student_bill` ON `acc_transection_type`.`transection_type_id`
                    = `ems_student_bill`.`transection_type`
                WHERE
                  `ems_student_bill`.`student_id` = '$this->studentID' AND
                  `ems_student_bill`.`semester` = '$this->semester' AND
                  `ems_student_bill`.`year` = '$this->year'";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $_allData[] = $row;
        }
        return $_allData;
    }
}