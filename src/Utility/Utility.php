<?php
namespace App\Utility;

class Utility
{
    public static function d($data=NULL){
        echo "<pre>";
        var_dump($data);
        echo "<pre>";
    }

    public static function dd($data=NULL){
        echo "<pre>";
        var_dump($data);
        echo "<pre>";
        die();
    }
    public static function redirect($data=NULL){
        header('Location:'.$data);
    }

    public static function validation($data=NULL){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
}